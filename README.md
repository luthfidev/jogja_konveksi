# jogja_konveksi

# Trigger

CREATE TRIGGER `bordir_log` AFTER INSERT ON `tb_bordir`
 FOR EACH ROW begin
insert into tb_bordir_log(br_id, br_kode, pelanggan_id_ct, br_nama_pesanan, br_jumlah, br_stitch, br_total_stitch, br_datang, br_kirim, br_harga, br_total_harga, new.br_tanggal) values (new.br_id, new.br_kode, new.pelanggan_id_ct, new.br_nama_pesanan, new.br_jumlah, new.br_stitch, new.br_total_stitch, new.br_datang, new.br_kirim, new.br_harga, new.br_total_harga, new.br_tanggal);
end

CREATE TRIGGER `delete_trans_sablon` AFTER DELETE ON `tb_sablon`
 FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
    WHERE tb_transaksi.tr_kode = old.sb_kode;
END

CREATE TRIGGER `delete_trans_bahan` AFTER DELETE ON `tb_bahan`
 FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
    WHERE tb_transaksi.tr_kode = old.bh_kode;
END

CREATE TRIGGER `delete_trans_bordir` AFTER DELETE ON `tb_bordir`
 FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
    WHERE tb_transaksi.tr_kode = old.br_kode;
END

CREATE TRIGGER `pelanggan_log` AFTER INSERT ON `tb_pelanggan`
 FOR EACH ROW begin
insert into tb_pelanggan_log(ct_id, ct_kode, ct_nama, ct_alamat, ct_telepon, ct_tanggal) values (new.ct_id, new.ct_kode, new.ct_nama, new.ct_alamat, new.ct_telepon, new.ct_tanggal);
END

CREATE TRIGGER `delete_trans_sublime` AFTER DELETE ON `tb_sublime`
 FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
    WHERE tb_transaksi.tr_kode = old.sbl_kode;
END

CREATE TRIGGER `sablon_log` AFTER INSERT ON `tb_sablon`
 FOR EACH ROW begin
insert into tb_sablon_log(sb_id, sb_kode, pelanggan_id_ct, sb_nama_pesanan, sb_jumlah, sb_stitch, sb_total_stitch, sb_datang, sb_kirim, sb_harga, sb_total_harga, sb_tanggal) values (new.sb_id, new.sb_kode, new.pelanggan_id_ct, new.sb_nama_pesanan, new.sb_jumlah, new.sb_stitch, new.sb_total_stitch, new.sb_datang, new.sb_kirim, new.sb_harga, new.sb_total_harga, new.sb_tanggal);
end

CREATE TRIGGER `sublime_log` AFTER INSERT ON `tb_sublime`
 FOR EACH ROW begin
insert into tb_sublime_log(sbl_id, sbl_kode, pelanggan_id_ct, sbl_nama_pesanan, sbl_jumlah, sbl_stitch, sbl_total_stitch, sbl_datang, sbl_kirim, sbl_harga, sbl_total_harga, sbl_tanggal) values (new.sbl_id, new.sbl_kode, new.pelanggan_id_ct, new.sbl_nama_pesanan, new.sbl_jumlah, new.sbl_stitch, new.sbl_total_stitch, new.sbl_datang, new.sbl_kirim, new.sbl_harga, new.sbl_total_harga, new.sbl_tanggal);
END

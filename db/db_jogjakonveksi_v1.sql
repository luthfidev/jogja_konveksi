-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 12, 2019 at 08:46 AM
-- Server version: 5.7.17-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_jogjakonveksi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_bordir`
--

CREATE TABLE `tb_bordir` (
  `br_id` int(11) NOT NULL,
  `br_kode` varchar(100) NOT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `br_nama_pesanan` varchar(200) NOT NULL,
  `br_jumlah` int(11) NOT NULL,
  `br_stitch` int(11) NOT NULL,
  `br_total_stitch` int(11) NOT NULL,
  `br_datang` date NOT NULL,
  `br_kirim` date NOT NULL,
  `br_harga` bigint(20) NOT NULL,
  `br_total_harga` bigint(20) NOT NULL,
  `br_tanggal` date NOT NULL,
  `br_created_name` varchar(100) DEFAULT NULL,
  `br_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `tb_bordir`
--
DELIMITER $$
CREATE TRIGGER `bordir_log` AFTER INSERT ON `tb_bordir` FOR EACH ROW begin
insert into tb_bordir_log(br_id, br_kode, pelanggan_id_ct, br_nama_pesanan, br_jumlah, br_stitch, br_total_stitch, br_datang, br_kirim, br_harga, br_total_harga, new.br_tanggal) values (new.br_id, new.br_kode, new.pelanggan_id_ct, new.br_nama_pesanan, new.br_jumlah, new.br_stitch, new.br_total_stitch, new.br_datang, new.br_kirim, new.br_harga, new.br_total_harga, new.br_tanggal);
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `delete_trans_bordir` AFTER DELETE ON `tb_bordir` FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
    WHERE tb_transaksi.tr_kode = old.br_kode;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bordir_log`
--

CREATE TABLE `tb_bordir_log` (
  `br_id_log` int(11) NOT NULL,
  `br_id` int(11) NOT NULL,
  `br_kode` varchar(100) DEFAULT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `br_nama_pesanan` varchar(200) NOT NULL,
  `br_jumlah` int(11) NOT NULL,
  `br_stitch` int(11) NOT NULL,
  `br_total_stitch` int(11) NOT NULL,
  `br_datang` date NOT NULL,
  `br_kirim` date NOT NULL,
  `br_harga` bigint(20) NOT NULL,
  `br_total_harga` bigint(20) NOT NULL,
  `br_tanggal` date NOT NULL,
  `br_created_name` varchar(100) DEFAULT NULL,
  `br_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_bordir_log`
--

INSERT INTO `tb_bordir_log` (`br_id_log`, `br_id`, `br_kode`, `pelanggan_id_ct`, `br_nama_pesanan`, `br_jumlah`, `br_stitch`, `br_total_stitch`, `br_datang`, `br_kirim`, `br_harga`, `br_total_harga`, `br_tanggal`, `br_created_name`, `br_created_time`) VALUES
(1, 4, 'BR510102019003', 3, 'aa', 25, 12, 12, '2019-10-11', '2019-10-24', 20000, 10000000, '0000-00-00', NULL, '2019-10-10 23:01:21'),
(2, 5, 'BR510102019004', 3, 'rompi', 12, 12, 2, '2019-10-17', '2019-10-11', 30000, 500000, '0000-00-00', NULL, '2019-10-10 23:02:44'),
(3, 6, 'BR512102019001', 2, 'kamar', 2, 2, 20, '2019-10-25', '2019-10-04', 20000, 30000000, '0000-00-00', NULL, '2019-10-12 08:37:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `ct_id` int(11) NOT NULL,
  `ct_kode` varchar(200) NOT NULL,
  `ct_nama` varchar(200) NOT NULL,
  `ct_alamat` text NOT NULL,
  `ct_telepon` varchar(15) NOT NULL,
  `ct_tanggal` date NOT NULL,
  `ct_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`ct_id`, `ct_kode`, `ct_nama`, `ct_alamat`, `ct_telepon`, `ct_tanggal`, `ct_created_time`) VALUES
(1, 'CT507102019001', 'Kamal', 'kotagede', '1234', '0000-00-00', '2019-10-10 15:44:11'),
(2, 'CT507102019002', 'Indrasd', 'Jogja', '1234', '0000-00-00', '2019-10-10 16:34:52'),
(3, 'CT510102019003', 'Hendrass', 'Patukan', '123456', '0000-00-00', '2019-10-10 16:31:08');

--
-- Triggers `tb_pelanggan`
--
DELIMITER $$
CREATE TRIGGER `pelanggan_log` AFTER INSERT ON `tb_pelanggan` FOR EACH ROW begin
insert into tb_pelanggan_log(ct_id, ct_kode, ct_nama, ct_alamat, ct_telepon, ct_tanggal) values (new.ct_id, new.ct_kode, new.ct_nama, new.ct_alamat, new.ct_telepon, new.ct_tanggal);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelanggan_log`
--

CREATE TABLE `tb_pelanggan_log` (
  `ct_id_log` int(11) NOT NULL,
  `ct_id` int(11) NOT NULL,
  `ct_kode` varchar(200) NOT NULL,
  `ct_nama` varchar(200) NOT NULL,
  `ct_alamat` text NOT NULL,
  `ct_telepon` varchar(15) NOT NULL,
  `ct_tanggal` date NOT NULL,
  `ct_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_pelanggan_log`
--

INSERT INTO `tb_pelanggan_log` (`ct_id_log`, `ct_id`, `ct_kode`, `ct_nama`, `ct_alamat`, `ct_telepon`, `ct_tanggal`, `ct_created_time`) VALUES
(1, 3, 'CT510102019003', 'Hendra', 'Patukan', '123456', '0000-00-00', '2019-10-10 15:44:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sablon`
--

CREATE TABLE `tb_sablon` (
  `sb_id` int(11) NOT NULL,
  `sb_kode` varchar(100) NOT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `sb_nama_pesanan` varchar(200) NOT NULL,
  `sb_jumlah` int(11) NOT NULL,
  `sb_stitch` int(11) NOT NULL,
  `sb_total_stitch` int(11) NOT NULL,
  `sb_datang` date NOT NULL,
  `sb_kirim` date NOT NULL,
  `sb_harga` bigint(20) NOT NULL,
  `sb_total_harga` bigint(20) NOT NULL,
  `sb_tanggal` date NOT NULL,
  `sb_created_name` varchar(100) DEFAULT NULL,
  `sb_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `tb_sablon`
--
DELIMITER $$
CREATE TRIGGER `delete_trans_sablon` AFTER DELETE ON `tb_sablon` FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
    WHERE tb_transaksi.tr_kode = old.sb_kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sablon_log` AFTER INSERT ON `tb_sablon` FOR EACH ROW begin
insert into tb_sablon_log(sb_id, sb_kode, pelanggan_id_ct, sb_nama_pesanan, sb_jumlah, sb_stitch, sb_total_stitch, sb_datang, sb_kirim, sb_harga, sb_total_harga, sb_tanggal) values (new.sb_id, new.sb_kode, new.pelanggan_id_ct, new.sb_nama_pesanan, new.sb_jumlah, new.sb_stitch, new.sb_total_stitch, new.sb_datang, new.sb_kirim, new.sb_harga, new.sb_total_harga, new.sb_tanggal);
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_sablon_log`
--

CREATE TABLE `tb_sablon_log` (
  `sb_id_log` int(11) NOT NULL,
  `sb_id` int(11) NOT NULL,
  `sb_kode` varchar(100) DEFAULT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `sb_nama_pesanan` varchar(200) NOT NULL,
  `sb_jumlah` int(11) NOT NULL,
  `sb_stitch` int(11) NOT NULL,
  `sb_total_stitch` int(11) NOT NULL,
  `sb_datang` date NOT NULL,
  `sb_kirim` date NOT NULL,
  `sb_harga` bigint(20) NOT NULL,
  `sb_total_harga` bigint(20) NOT NULL,
  `sb_tanggal` date NOT NULL,
  `sb_created_name` varchar(100) DEFAULT NULL,
  `sb_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_sablon_log`
--

INSERT INTO `tb_sablon_log` (`sb_id_log`, `sb_id`, `sb_kode`, `pelanggan_id_ct`, `sb_nama_pesanan`, `sb_jumlah`, `sb_stitch`, `sb_total_stitch`, `sb_datang`, `sb_kirim`, `sb_harga`, `sb_total_harga`, `sb_tanggal`, `sb_created_name`, `sb_created_time`) VALUES
(1, 3, 'SB510102019001', 2, 'smk', 2, 20, 20, '2019-10-17', '2019-10-17', 20000, 2000000, '0000-00-00', NULL, '2019-10-10 15:37:13'),
(2, 1, 'SB512102019001', 3, 'smk', 2, 2, 2, '2019-10-11', '2019-10-09', 2000, 20000, '2019-10-12', NULL, '2019-10-12 04:41:51'),
(3, 2, 'SB512102019002', 3, 'SMK', 1, 1, 1, '2019-10-19', '2019-10-18', 2000, 2000, '2019-10-12', NULL, '2019-10-12 06:28:00'),
(4, 3, 'SB512102019003', 3, 'SMK', 1, 1, 1, '2019-10-19', '2019-10-18', 2000, 2000, '2019-10-12', NULL, '2019-10-12 06:28:12'),
(5, 4, 'SB512102019004', 3, 'jogja', 3, 2, 2, '2019-10-23', '2019-10-09', 40000, 5000000, '2019-10-12', NULL, '2019-10-12 06:58:25'),
(6, 2, 'SB512102019001', 2, 's', 2, 2, 2, '2019-10-24', '2019-10-09', 2, 2, '2019-10-12', NULL, '2019-10-12 08:33:12'),
(7, 3, 'SB512102019001', 3, 'jogja', 22, 2, 2, '2019-10-24', '2019-10-11', 2000, 20000, '2019-10-12', NULL, '2019-10-12 08:36:04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sublime`
--

CREATE TABLE `tb_sublime` (
  `sbl_id` int(11) NOT NULL,
  `sbl_kode` varchar(100) NOT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `sbl_nama_pesanan` varchar(200) NOT NULL,
  `sbl_jumlah` int(11) NOT NULL,
  `sbl_stitch` int(11) NOT NULL,
  `sbl_total_stitch` int(11) NOT NULL,
  `sbl_datang` date NOT NULL,
  `sbl_kirim` date NOT NULL,
  `sbl_harga` bigint(20) NOT NULL,
  `sbl_total_harga` bigint(20) NOT NULL,
  `sbl_tanggal` date NOT NULL,
  `sbl_created_name` varchar(100) DEFAULT NULL,
  `sbl_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `tb_sublime`
--
DELIMITER $$
CREATE TRIGGER `delete_trans_sublime` AFTER DELETE ON `tb_sublime` FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
    WHERE tb_transaksi.tr_kode = old.sbl_kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sublime_log` AFTER INSERT ON `tb_sublime` FOR EACH ROW begin
insert into tb_sublime_log(sbl_id, sbl_kode, pelanggan_id_ct, sbl_nama_pesanan, sbl_jumlah, sbl_stitch, sbl_total_stitch, sbl_datang, sbl_kirim, sbl_harga, sbl_total_harga, sbl_tanggal) values (new.sbl_id, new.sbl_kode, new.pelanggan_id_ct, new.sbl_nama_pesanan, new.sbl_jumlah, new.sbl_stitch, new.sbl_total_stitch, new.sbl_datang, new.sbl_kirim, new.sbl_harga, new.sbl_total_harga, new.sbl_tanggal);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_sublime_log`
--

CREATE TABLE `tb_sublime_log` (
  `sbl_id_log` int(11) NOT NULL,
  `sbl_id` int(11) NOT NULL,
  `sbl_kode` varchar(100) DEFAULT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `sbl_nama_pesanan` varchar(200) NOT NULL,
  `sbl_jumlah` int(11) NOT NULL,
  `sbl_stitch` int(11) NOT NULL,
  `sbl_total_stitch` int(11) NOT NULL,
  `sbl_datang` date NOT NULL,
  `sbl_kirim` date NOT NULL,
  `sbl_harga` bigint(20) NOT NULL,
  `sbl_total_harga` bigint(20) NOT NULL,
  `sbl_tanggal` date NOT NULL,
  `sbl_created_name` varchar(100) DEFAULT NULL,
  `sbl_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_sublime_log`
--

INSERT INTO `tb_sublime_log` (`sbl_id_log`, `sbl_id`, `sbl_kode`, `pelanggan_id_ct`, `sbl_nama_pesanan`, `sbl_jumlah`, `sbl_stitch`, `sbl_total_stitch`, `sbl_datang`, `sbl_kirim`, `sbl_harga`, `sbl_total_harga`, `sbl_tanggal`, `sbl_created_name`, `sbl_created_time`) VALUES
(1, 1, 'SBL512102019001', 1, 'Bordir Nama', 2, 12, 12, '2019-10-10', '2019-10-12', 2000, 24000, '2019-10-12', NULL, '2019-10-12 07:55:53'),
(2, 1, 'SBL512102019001', 3, 'smk muhi', 2, 12, 12, '2019-10-25', '2019-10-24', 2000, 200000, '2019-10-12', NULL, '2019-10-12 08:03:34'),
(3, 2, 'SBL512102019001', 3, 'jarik', 2, 2, 20, '2019-10-25', '2019-10-17', 2000, 2000000, '2019-10-12', NULL, '2019-10-12 08:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `tr_id` int(11) NOT NULL,
  `tr_kode` varchar(100) NOT NULL,
  `tr_nama` varchar(100) NOT NULL,
  `tr_jenis` enum('1','2') NOT NULL,
  `tr_jumlah` int(11) NOT NULL,
  `tr_harga` bigint(20) NOT NULL,
  `tr_total_harga` bigint(20) NOT NULL,
  `tr_tanggal` date NOT NULL,
  `tr_created_name` varchar(100) DEFAULT NULL,
  `tr_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_bordir`
--
ALTER TABLE `tb_bordir`
  ADD PRIMARY KEY (`br_id`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`),
  ADD KEY `br_kode` (`br_kode`);

--
-- Indexes for table `tb_bordir_log`
--
ALTER TABLE `tb_bordir_log`
  ADD PRIMARY KEY (`br_id_log`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`);

--
-- Indexes for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`ct_id`);

--
-- Indexes for table `tb_pelanggan_log`
--
ALTER TABLE `tb_pelanggan_log`
  ADD PRIMARY KEY (`ct_id_log`);

--
-- Indexes for table `tb_sablon`
--
ALTER TABLE `tb_sablon`
  ADD PRIMARY KEY (`sb_id`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`),
  ADD KEY `sb_kode` (`sb_kode`);

--
-- Indexes for table `tb_sablon_log`
--
ALTER TABLE `tb_sablon_log`
  ADD PRIMARY KEY (`sb_id_log`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`);

--
-- Indexes for table `tb_sublime`
--
ALTER TABLE `tb_sublime`
  ADD PRIMARY KEY (`sbl_id`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`),
  ADD KEY `sbl_kode` (`sbl_kode`);

--
-- Indexes for table `tb_sublime_log`
--
ALTER TABLE `tb_sublime_log`
  ADD PRIMARY KEY (`sbl_id_log`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`);

--
-- Indexes for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`tr_id`),
  ADD KEY `tr_kode` (`tr_kode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_bordir`
--
ALTER TABLE `tb_bordir`
  MODIFY `br_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_bordir_log`
--
ALTER TABLE `tb_bordir_log`
  MODIFY `br_id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  MODIFY `ct_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_pelanggan_log`
--
ALTER TABLE `tb_pelanggan_log`
  MODIFY `ct_id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_sablon`
--
ALTER TABLE `tb_sablon`
  MODIFY `sb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_sablon_log`
--
ALTER TABLE `tb_sablon_log`
  MODIFY `sb_id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_sublime`
--
ALTER TABLE `tb_sublime`
  MODIFY `sbl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_sublime_log`
--
ALTER TABLE `tb_sublime_log`
  MODIFY `sbl_id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `tr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

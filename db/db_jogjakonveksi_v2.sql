-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 22 Des 2019 pada 23.50
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_jogjakonveksi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('n0ntf311t3vlgkfgbfpkn48v76eukmhl', '::1', 1575940365, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934303336353b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('a70r0670mte3qbpf3me8q4divcvbt6sa', '::1', 1575944670, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934343637303b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('akfrqeo93rjk07gal6ts9efol7et2l6b', '::1', 1575945056, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934353035363b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('h3khnujk6hmjkr58m9ktj3viuuufkm9r', '::1', 1575945364, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934353336343b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('ichhg8hdd8ubnormiloll716bn2i5bsj', '::1', 1575945667, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934353636373b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('bn26071lg7ltkvbum3q8u7v9v9u5nl7j', '::1', 1575946150, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934363135303b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('h8kiriq12jbpfcsh0ppmea1ehd445516', '::1', 1575948173, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934383137333b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('ahjttkduabmqm1dlbni9h1227ki6uo71', '::1', 1575948511, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934383531313b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('vhlgnecb3o61m4gfbt5jhqo4li8cc5g3', '::1', 1575948821, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934383832313b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('ak2rb8s30sttp0mdr7ssv18piuddsp3k', '::1', 1575949064, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353934383832313b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('7b5nmhg2ia23ghbu4s431hp4qolvnna2', '::1', 1575975775, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353937353737353b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('c9j6bokf28r8d8ruq3cik51q7u5e9hk2', '::1', 1575975903, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353937353737353b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('omclttpv998al8gbgi4strqnfiofakb3', '::1', 1575987345, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353938373334353b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('5ohqt6q7gmdha15h2pgkdm9q61dl1d5q', '::1', 1575988108, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353938383130383b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('b5d0p1e45ss562r45l66euoqr596pe81', '::1', 1575988293, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353938383130383b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b),
('epr7ptc5so8o26v1heqpa0qqunch8agg', '::1', 1576023573, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537363032333533393b6d6173756b7c623a313b6c6576656c5f69647c733a313a2231223b69645f757365727c733a313a2231223b757365726e616d657c733a353a2261646d696e223b6e616d615f757365727c4e3b70617373776f72647c733a33323a223231323332663239376135376135613734333839346130653461383031666333223b69645f746168756e7c733a343a2232303139223b);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_bahan`
--

CREATE TABLE `tb_bahan` (
  `bh_id` int(11) NOT NULL,
  `bh_kode` varchar(100) NOT NULL,
  `bh_nama` varchar(100) NOT NULL,
  `bh_jenis` enum('1','2') NOT NULL,
  `bh_jumlah` int(11) NOT NULL,
  `bh_harga` bigint(20) NOT NULL,
  `bh_total_harga` bigint(20) NOT NULL,
  `bh_tanggal` date NOT NULL,
  `bh_created_name` varchar(100) DEFAULT NULL,
  `bh_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_bordir`
--

CREATE TABLE `tb_bordir` (
  `br_id` int(11) NOT NULL,
  `br_kode` varchar(100) NOT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `br_nama_pesanan` varchar(200) NOT NULL,
  `br_jumlah` int(11) NOT NULL,
  `br_stitch` int(11) NOT NULL,
  `br_total_stitch` int(11) NOT NULL,
  `br_datang` date NOT NULL,
  `br_kirim` date NOT NULL,
  `br_harga` bigint(20) NOT NULL,
  `br_total_harga` bigint(20) NOT NULL,
  `br_status` enum('Belum Lunas','Lunas') DEFAULT NULL,
  `br_tahun` year(4) NOT NULL,
  `br_tanggal` date NOT NULL,
  `br_created_name` varchar(100) DEFAULT NULL,
  `br_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Trigger `tb_bordir`
--
DELIMITER $$
CREATE TRIGGER `delete_transaksi_bordir` AFTER DELETE ON `tb_bordir` FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
	WHERE tb_transaksi.tr_kode = old.br_kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_status_bordir` AFTER UPDATE ON `tb_bordir` FOR EACH ROW UPDATE tb_transaksi set tb_transaksi.tr_status = new.br_status, tb_transaksi.tr_jumlah = new.br_jumlah, tb_transaksi.tr_harga = new.br_harga, tb_transaksi.tr_total_harga = new.br_total_harga WHERE tb_transaksi.tr_kode = old.br_kode
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_bordir_log`
--

CREATE TABLE `tb_bordir_log` (
  `br_id_log` int(11) NOT NULL,
  `br_id` int(11) NOT NULL,
  `br_kode` varchar(100) DEFAULT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `br_nama_pesanan` varchar(200) NOT NULL,
  `br_jumlah` int(11) NOT NULL,
  `br_stitch` int(11) NOT NULL,
  `br_total_stitch` int(11) NOT NULL,
  `br_datang` date NOT NULL,
  `br_kirim` date NOT NULL,
  `br_harga` bigint(20) NOT NULL,
  `br_total_harga` bigint(20) NOT NULL,
  `br_status` enum('Pending','Proses','Selesai','') DEFAULT NULL,
  `br_tanggal` date NOT NULL,
  `br_created_name` varchar(100) DEFAULT NULL,
  `br_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_bordir_log`
--

INSERT INTO `tb_bordir_log` (`br_id_log`, `br_id`, `br_kode`, `pelanggan_id_ct`, `br_nama_pesanan`, `br_jumlah`, `br_stitch`, `br_total_stitch`, `br_datang`, `br_kirim`, `br_harga`, `br_total_harga`, `br_status`, `br_tanggal`, `br_created_name`, `br_created_time`) VALUES
(1, 4, 'BR510102019003', 3, 'aa', 25, 12, 12, '2019-10-11', '2019-10-24', 20000, 10000000, NULL, '0000-00-00', NULL, '2019-10-10 23:01:21'),
(2, 5, 'BR510102019004', 3, 'rompi', 12, 12, 2, '2019-10-17', '2019-10-11', 30000, 500000, NULL, '0000-00-00', NULL, '2019-10-10 23:02:44'),
(3, 6, 'BR512102019001', 2, 'kamar', 2, 2, 20, '2019-10-25', '2019-10-04', 20000, 30000000, NULL, '0000-00-00', NULL, '2019-10-12 08:37:20'),
(4, 7, 'BR512102019001', 2, 'ddd', 2000, 22, 222, '2019-10-12', '2019-10-12', 100000, 1000000, NULL, '0000-00-00', NULL, '2019-10-12 11:44:03'),
(5, 8, 'BR512102019002', 5, 'Emblem', 12, 123456, 1523698, '2019-10-12', '2019-10-14', 20000, 24000, NULL, '0000-00-00', NULL, '2019-10-12 11:46:53'),
(6, 1, 'BR516102019001', 9, 'sd', 2, 2, 4, '2019-10-10', '2019-10-18', 2, 4, NULL, '0000-00-00', NULL, '2019-10-16 15:52:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_level`
--

CREATE TABLE `tb_level` (
  `id_level` smallint(1) NOT NULL,
  `nama_level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_level`
--

INSERT INTO `tb_level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'Operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `ct_id` int(11) NOT NULL,
  `ct_kode` varchar(200) NOT NULL,
  `ct_nama` varchar(200) NOT NULL,
  `ct_alamat` text NOT NULL,
  `ct_telepon` varchar(15) NOT NULL,
  `ct_tanggal` date NOT NULL,
  `ct_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`ct_id`, `ct_kode`, `ct_nama`, `ct_alamat`, `ct_telepon`, `ct_tanggal`, `ct_created_time`) VALUES
(1, 'CT507102019001', 'Kamal', 'kotagede', '1234', '0000-00-00', '2019-10-10 15:44:11'),
(2, 'CT507102019002', 'Indrasd', 'Jogja', '1234', '0000-00-00', '2019-10-10 16:34:52'),
(3, 'CT510102019003', 'Hendrass', 'Patukan', '123456', '0000-00-00', '2019-10-10 16:31:08'),
(4, 'CT512102019004', 'Indra', 'Jogja', '098877', '2019-10-12', '2019-10-12 11:43:31'),
(5, 'CT512102019005', 'Indra65', 'Jogja', '098877', '2019-10-12', '2019-10-12 11:44:52'),
(6, 'CT516102019006', 'ss', 'ss', '2222', '2019-10-16', '2019-10-16 15:35:18'),
(7, 'CT516102019007', 'agus', 'jogja', '123', '2019-10-16', '2019-10-16 15:37:03'),
(8, 'CT516102019008', 'asd', 'asda', '222', '2019-10-16', '2019-10-16 15:38:12'),
(9, 'CT516102019009', 'asd', 'asd', 'asdasd', '2019-10-16', '2019-10-16 15:42:02'),
(10, 'CT516102019010', 'sd', 'sd', 'sdsdd', '2019-10-16', '2019-10-16 15:53:37'),
(11, 'CT516102019011', 'asd', 'asd', 'asdasd', '2019-10-16', '2019-10-16 15:54:51'),
(12, 'C2019012', 'Janat', 'pleret', '08888', '2019-12-10', '2019-12-10 02:41:07'),
(13, 'C2019013', 'Romi', 'Pogung', '0888', '2019-12-10', '2019-12-10 02:41:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pelanggan_log`
--

CREATE TABLE `tb_pelanggan_log` (
  `ct_id_log` int(11) NOT NULL,
  `ct_id` int(11) NOT NULL,
  `ct_kode` varchar(200) NOT NULL,
  `ct_nama` varchar(200) NOT NULL,
  `ct_alamat` text NOT NULL,
  `ct_telepon` varchar(15) NOT NULL,
  `ct_tanggal` date NOT NULL,
  `ct_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_pelanggan_log`
--

INSERT INTO `tb_pelanggan_log` (`ct_id_log`, `ct_id`, `ct_kode`, `ct_nama`, `ct_alamat`, `ct_telepon`, `ct_tanggal`, `ct_created_time`) VALUES
(1, 3, 'CT510102019003', 'Hendra', 'Patukan', '123456', '0000-00-00', '2019-10-10 15:44:59'),
(2, 4, 'CT512102019004', 'Indra', 'Jogja', '098877', '2019-10-12', '2019-10-12 11:43:31'),
(3, 5, 'CT512102019005', 'Indra', 'Jogja', '098877', '2019-10-12', '2019-10-12 11:44:06'),
(4, 6, 'CT516102019006', 'ss', 'ss', '2222', '2019-10-16', '2019-10-16 15:35:18'),
(5, 7, 'CT516102019007', 'agus', 'jogja', '123', '2019-10-16', '2019-10-16 15:37:03'),
(6, 8, 'CT516102019008', 'asd', 'asda', '222', '2019-10-16', '2019-10-16 15:38:12'),
(7, 9, 'CT516102019009', 'asd', 'asd', 'asdasd', '2019-10-16', '2019-10-16 15:42:02'),
(8, 10, 'CT516102019010', 'sd', 'sd', 'sdsdd', '2019-10-16', '2019-10-16 15:53:37'),
(9, 11, 'CT516102019011', 'asd', 'asd', 'asdasd', '2019-10-16', '2019-10-16 15:54:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sablon`
--

CREATE TABLE `tb_sablon` (
  `sb_id` int(11) NOT NULL,
  `sb_kode` varchar(100) NOT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `sb_nama_pesanan` varchar(200) NOT NULL,
  `sb_jumlah` int(11) NOT NULL,
  `sb_datang` date NOT NULL,
  `sb_kirim` date NOT NULL,
  `sb_harga` bigint(20) NOT NULL,
  `sb_total_harga` bigint(20) NOT NULL,
  `sb_status` enum('Belum Lunas','Lunas') DEFAULT NULL,
  `sb_tahun` year(4) NOT NULL,
  `sb_tanggal` date NOT NULL,
  `sb_created_name` varchar(100) DEFAULT NULL,
  `sb_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Trigger `tb_sablon`
--
DELIMITER $$
CREATE TRIGGER `delete_transaksi_sablon` AFTER DELETE ON `tb_sablon` FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
	WHERE tb_transaksi.tr_kode = old.sb_kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_status_sablon` AFTER UPDATE ON `tb_sablon` FOR EACH ROW UPDATE tb_transaksi set tb_transaksi.tr_status = new.sb_status, tb_transaksi.tr_jumlah = new.sb_jumlah, tb_transaksi.tr_harga = new.sb_harga, tb_transaksi.tr_total_harga = new.sb_total_harga WHERE tb_transaksi.tr_kode = old.sb_kode
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sablon_log`
--

CREATE TABLE `tb_sablon_log` (
  `sb_id_log` int(11) NOT NULL,
  `sb_id` int(11) NOT NULL,
  `sb_kode` varchar(100) DEFAULT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `sb_nama_pesanan` varchar(200) NOT NULL,
  `sb_jumlah` int(11) NOT NULL,
  `sb_stitch` int(11) DEFAULT NULL,
  `sb_total_stitch` int(11) DEFAULT NULL,
  `sb_datang` date NOT NULL,
  `sb_kirim` date NOT NULL,
  `sb_harga` bigint(20) NOT NULL,
  `sb_total_harga` bigint(20) NOT NULL,
  `sb_status` enum('Pending','Proses','Selesai','') DEFAULT NULL,
  `sb_tanggal` date NOT NULL,
  `sb_created_name` varchar(100) DEFAULT NULL,
  `sb_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_sablon_log`
--

INSERT INTO `tb_sablon_log` (`sb_id_log`, `sb_id`, `sb_kode`, `pelanggan_id_ct`, `sb_nama_pesanan`, `sb_jumlah`, `sb_stitch`, `sb_total_stitch`, `sb_datang`, `sb_kirim`, `sb_harga`, `sb_total_harga`, `sb_status`, `sb_tanggal`, `sb_created_name`, `sb_created_time`) VALUES
(1, 3, 'SB510102019001', 2, 'smk', 2, 20, 20, '2019-10-17', '2019-10-17', 20000, 2000000, NULL, '0000-00-00', NULL, '2019-10-10 15:37:13'),
(2, 1, 'SB512102019001', 3, 'smk', 2, 2, 2, '2019-10-11', '2019-10-09', 2000, 20000, NULL, '2019-10-12', NULL, '2019-10-12 04:41:51'),
(3, 2, 'SB512102019002', 3, 'SMK', 1, 1, 1, '2019-10-19', '2019-10-18', 2000, 2000, NULL, '2019-10-12', NULL, '2019-10-12 06:28:00'),
(4, 3, 'SB512102019003', 3, 'SMK', 1, 1, 1, '2019-10-19', '2019-10-18', 2000, 2000, NULL, '2019-10-12', NULL, '2019-10-12 06:28:12'),
(5, 4, 'SB512102019004', 3, 'jogja', 3, 2, 2, '2019-10-23', '2019-10-09', 40000, 5000000, NULL, '2019-10-12', NULL, '2019-10-12 06:58:25'),
(6, 2, 'SB512102019001', 2, 's', 2, 2, 2, '2019-10-24', '2019-10-09', 2, 2, NULL, '2019-10-12', NULL, '2019-10-12 08:33:12'),
(7, 3, 'SB512102019001', 3, 'jogja', 22, 2, 2, '2019-10-24', '2019-10-11', 2000, 20000, NULL, '2019-10-12', NULL, '2019-10-12 08:36:04'),
(8, 4, 'SB512102019001', 3, 'bordir', 1, 2, 12, '2019-10-12', '2020-11-12', 20000, 2000, NULL, '2019-10-12', NULL, '2019-10-12 11:40:15'),
(9, 6, 'SB513102019001', 5, 'Bordir SMK', 3, NULL, NULL, '2019-10-16', '2019-10-16', 20000, 60000, NULL, '2019-10-13', NULL, '2019-10-13 07:55:55'),
(10, 7, 'SB514102019002', 5, 's', 2, NULL, NULL, '2019-10-14', '2019-10-14', 2222, 4444, NULL, '2019-10-14', NULL, '2019-10-14 15:06:59'),
(11, 8, 'SB516102019003', 5, '2', 25, NULL, NULL, '2019-10-18', '2019-10-10', 5000000, 125000000, NULL, '2019-10-16', NULL, '2019-10-16 05:22:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sublime`
--

CREATE TABLE `tb_sublime` (
  `sbl_id` int(11) NOT NULL,
  `sbl_kode` varchar(100) NOT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `sbl_nama_pesanan` varchar(200) NOT NULL,
  `sbl_jumlah` int(11) NOT NULL,
  `sbl_stitch` int(11) DEFAULT NULL,
  `sbl_total_stitch` int(11) DEFAULT NULL,
  `sbl_datang` date NOT NULL,
  `sbl_kirim` date NOT NULL,
  `sbl_harga` bigint(20) NOT NULL,
  `sbl_total_harga` bigint(20) NOT NULL,
  `sbl_status` enum('Belum Lunas','Lunas','') DEFAULT NULL,
  `sbl_tahun` year(4) NOT NULL,
  `sbl_tanggal` date NOT NULL,
  `sbl_created_name` varchar(100) DEFAULT NULL,
  `sbl_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Trigger `tb_sublime`
--
DELIMITER $$
CREATE TRIGGER `delete_transaksi_sublime` AFTER DELETE ON `tb_sublime` FOR EACH ROW BEGIN
DELETE FROM tb_transaksi
	WHERE tb_transaksi.tr_kode = old.sbl_kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_status_sublime` AFTER UPDATE ON `tb_sublime` FOR EACH ROW UPDATE tb_transaksi set tb_transaksi.tr_status = new.sbl_status, tb_transaksi.tr_jumlah = new.sbl_jumlah, tb_transaksi.tr_harga = new.sbl_harga, tb_transaksi.tr_total_harga = new.sbl_total_harga WHERE tb_transaksi.tr_kode = old.sbl_kode
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sublime_log`
--

CREATE TABLE `tb_sublime_log` (
  `sbl_id_log` int(11) NOT NULL,
  `sbl_id` int(11) NOT NULL,
  `sbl_kode` varchar(100) DEFAULT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `sbl_nama_pesanan` varchar(200) NOT NULL,
  `sbl_jumlah` int(11) NOT NULL,
  `sbl_stitch` int(11) DEFAULT NULL,
  `sbl_total_stitch` int(11) DEFAULT NULL,
  `sbl_datang` date NOT NULL,
  `sbl_kirim` date NOT NULL,
  `sbl_harga` bigint(20) NOT NULL,
  `sbl_total_harga` bigint(20) NOT NULL,
  `sbl_status` enum('Pending','Proses','Selesai','') DEFAULT NULL,
  `sbl_tanggal` date NOT NULL,
  `sbl_created_name` varchar(100) DEFAULT NULL,
  `sbl_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_sublime_log`
--

INSERT INTO `tb_sublime_log` (`sbl_id_log`, `sbl_id`, `sbl_kode`, `pelanggan_id_ct`, `sbl_nama_pesanan`, `sbl_jumlah`, `sbl_stitch`, `sbl_total_stitch`, `sbl_datang`, `sbl_kirim`, `sbl_harga`, `sbl_total_harga`, `sbl_status`, `sbl_tanggal`, `sbl_created_name`, `sbl_created_time`) VALUES
(1, 1, 'SBL512102019001', 1, 'Bordir Nama', 2, 12, 12, '2019-10-10', '2019-10-12', 2000, 24000, NULL, '2019-10-12', NULL, '2019-10-12 07:55:53'),
(2, 1, 'SBL512102019001', 3, 'smk muhi', 2, 12, 12, '2019-10-25', '2019-10-24', 2000, 200000, NULL, '2019-10-12', NULL, '2019-10-12 08:03:34'),
(3, 2, 'SBL512102019001', 3, 'jarik', 2, 2, 20, '2019-10-25', '2019-10-17', 2000, 2000000, NULL, '2019-10-12', NULL, '2019-10-12 08:36:29'),
(4, 3, 'SBL514102019001', 5, 'jogja', 20, NULL, NULL, '2019-10-09', '2019-10-23', 20000, 400000, NULL, '2019-10-14', NULL, '2019-10-14 22:43:33'),
(5, 4, 'SBL515102019002', 4, 'll', 20, NULL, NULL, '2019-10-23', '2019-10-22', 2000, 40000, NULL, '2019-10-15', NULL, '2019-10-15 05:43:01'),
(6, 5, 'SBL516102019003', 5, 'jaka', 20, NULL, NULL, '2019-10-15', '2019-10-10', 20000, 400000, NULL, '2019-10-16', NULL, '2019-10-16 01:17:21'),
(7, 6, 'SBL517102019004', 11, 's', 2, NULL, NULL, '2019-10-16', '2019-10-17', 2000, 4000, NULL, '2019-10-17', NULL, '2019-10-17 12:30:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tahun`
--

CREATE TABLE `tb_tahun` (
  `id_tahun` int(11) NOT NULL,
  `tahun` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_tahun`
--

INSERT INTO `tb_tahun` (`id_tahun`, `tahun`) VALUES
(1, '2019'),
(2, '2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `tr_id` int(11) NOT NULL,
  `tr_kode` varchar(100) NOT NULL,
  `pelanggan_id_ct` int(11) NOT NULL,
  `tr_nama` varchar(100) NOT NULL,
  `tr_produk` varchar(100) NOT NULL,
  `tr_jenis` enum('1','2') NOT NULL,
  `tr_stitch` int(11) DEFAULT NULL,
  `tr_total_stitch` int(11) DEFAULT NULL,
  `tr_jumlah` int(11) NOT NULL,
  `tr_harga` bigint(20) NOT NULL,
  `tr_total_harga` bigint(20) NOT NULL,
  `tr_status` enum('Belum Lunas','Lunas','') DEFAULT NULL,
  `tr_tahun` year(4) NOT NULL,
  `tr_datang` date DEFAULT NULL,
  `tr_kirim` date DEFAULT NULL,
  `tr_tanggal` date NOT NULL,
  `tr_created_name` varchar(100) DEFAULT NULL,
  `tr_created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` smallint(1) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level_id` smallint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `level_id`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
(5, 'kamal', 'aa63b0d5d950361c05012235ab520512', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indeks untuk tabel `tb_bahan`
--
ALTER TABLE `tb_bahan`
  ADD PRIMARY KEY (`bh_id`),
  ADD KEY `bh_kode` (`bh_kode`);

--
-- Indeks untuk tabel `tb_bordir`
--
ALTER TABLE `tb_bordir`
  ADD PRIMARY KEY (`br_id`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`),
  ADD KEY `br_kode` (`br_kode`);

--
-- Indeks untuk tabel `tb_bordir_log`
--
ALTER TABLE `tb_bordir_log`
  ADD PRIMARY KEY (`br_id_log`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`);

--
-- Indeks untuk tabel `tb_level`
--
ALTER TABLE `tb_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indeks untuk tabel `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`ct_id`);

--
-- Indeks untuk tabel `tb_pelanggan_log`
--
ALTER TABLE `tb_pelanggan_log`
  ADD PRIMARY KEY (`ct_id_log`);

--
-- Indeks untuk tabel `tb_sablon`
--
ALTER TABLE `tb_sablon`
  ADD PRIMARY KEY (`sb_id`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`);

--
-- Indeks untuk tabel `tb_sablon_log`
--
ALTER TABLE `tb_sablon_log`
  ADD PRIMARY KEY (`sb_id_log`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`);

--
-- Indeks untuk tabel `tb_sublime`
--
ALTER TABLE `tb_sublime`
  ADD PRIMARY KEY (`sbl_id`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`),
  ADD KEY `sbl_kode` (`sbl_kode`);

--
-- Indeks untuk tabel `tb_sublime_log`
--
ALTER TABLE `tb_sublime_log`
  ADD PRIMARY KEY (`sbl_id_log`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`);

--
-- Indeks untuk tabel `tb_tahun`
--
ALTER TABLE `tb_tahun`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indeks untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`tr_id`),
  ADD KEY `tr_kode` (`tr_kode`),
  ADD KEY `pelanggan_id_ct` (`pelanggan_id_ct`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `level_id` (`level_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_bahan`
--
ALTER TABLE `tb_bahan`
  MODIFY `bh_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_bordir`
--
ALTER TABLE `tb_bordir`
  MODIFY `br_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_bordir_log`
--
ALTER TABLE `tb_bordir_log`
  MODIFY `br_id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_level`
--
ALTER TABLE `tb_level`
  MODIFY `id_level` smallint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  MODIFY `ct_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tb_pelanggan_log`
--
ALTER TABLE `tb_pelanggan_log`
  MODIFY `ct_id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_sablon`
--
ALTER TABLE `tb_sablon`
  MODIFY `sb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_sablon_log`
--
ALTER TABLE `tb_sablon_log`
  MODIFY `sb_id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tb_sublime`
--
ALTER TABLE `tb_sublime`
  MODIFY `sbl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_sublime_log`
--
ALTER TABLE `tb_sublime_log`
  MODIFY `sbl_id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_tahun`
--
ALTER TABLE `tb_tahun`
  MODIFY `id_tahun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `tr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` smallint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

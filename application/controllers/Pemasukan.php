<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemasukan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		$this->load->helper('fungsi');
		$this->load->model('M_Pelanggan');
		$this->load->model('M_Sablon');
		$this->load->model('M_Sublime');
		$this->load->model('M_Bordir');
		$this->load->model('M_Count');
		$this->load->model('M_Transaksi');
	}

	public function index()
	{
		$data['kode'] = $this->M_Sablon->kode_otomatis();
		$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
		$this->template_admin->load('template_admin','pemasukan/sablon/index',$data);

	}

	public function data()
	{
		$data['dataSablon'] 	= $this->M_Sablon->data_sablon();
		$data['dataSublime'] 	= $this->M_Sublime->data_sublime();
		$data['dataBordir'] 	= $this->M_Bordir->data_bordir();
		$data['dataTransaksi'] 	= $this->M_Transaksi->data_transaksi_in();
		$x = $this->M_Count->chart_statistik_tahun()->result();
		$data['chart1'] = json_encode($x);
		$this->template_admin->load('template_admin','pemasukan/total/data',$data);

	}

	public function blmlunas()
	{
		$data['dataTagihan'] 			= $this->M_Count->data_tagihan();
		$this->template_admin->load('template_admin','pemasukan/blmlunas/data',$data);

	}

	public function delete() 
	{
		if($this->session->userdata("id_user")!=="" ) {
			$id = $this->uri->segment(3);
			$this->M_Transaksi->delete_transaksi($id);
			//$this->session->set_flashdata('hapus','Berhasil di Hapus !');
			redirect("pemasukan/data");
		}
		else{
			redirect('bordir');
		}
	}

}

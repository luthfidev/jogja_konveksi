<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		$this->load->model('M_Pelanggan');
	}

	public function index()
	{
		
		$this->template_admin->load('template_admin','pelanggan/tambah');

	}

	public function tambah()
	{
		$data['kode'] = $this->M_Pelanggan->kode_otomatis();
		$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
		$this->template_admin->load('template_admin','pelanggan/tambah',$data);

	}

	public function simpan()
	{
		if($this->session->userdata("id_user")!=="" ) {

			$this->form_validation->set_rules('ct_kode','Kode Pelanggan','required');
			$this->form_validation->set_rules('ct_nama','Nama Pelanggan','required');
			$this->form_validation->set_rules('ct_alamat','Golongan','required');

			if ($this->form_validation->run()==FALSE) {
				$this->session->set_flashdata('datagagal',"Data Gagal Di Simpan");
				$this->template_admin->load('template_admin','pelanggan/tambah',$data);

			}
			else {
					$today					= date("Y/m/d");
					$in['ct_kode']			= $this->input->post('ct_kode');
	$kode_exist = $in['ct_nama'] 			= $this->input->post('ct_nama');
					$in['ct_alamat'] 		= $this->input->post('ct_alamat');
					$in['ct_telepon'] 		= $this->input->post('ct_telepon');
					$in['ct_tanggal']		= $today;					
							

					$sql = $this->db->query("SELECT ct_kode FROM tb_pelanggan where ct_kode='$kode_exist'");
					$cek_kode_exist = $sql->num_rows();
					if ($cek_kode_exist > 0) {
					$this->session->set_flashdata('exist', 'NIP Sudah Ada');	
					redirect('pelanggan/tambah');
					} else {				
					$this->db->insert("tb_pelanggan",$in);
							
					$this->session->set_flashdata('datasukses',"Data Berhasil Disimpan");
					redirect("pelanggan/tambah");
			}

		}

	}
		else{
			redirect('auth');

		}

	}

	function ubah()
	{	
		$id = $this->uri->segment(3);
		$data['row'] = $this->M_Pelanggan->get_pelanggan($id)->row();
		$this->template_admin->load('template_admin','pelanggan/ubah',$data);
	}

	public function update() {

		if($this->session->userdata("id_user")!=="" ) {

			$this->form_validation->set_rules('ct_kode','Kode','required');
			$this->form_validation->set_rules('ct_nama','Nama Pelanggan','required');
			$this->form_validation->set_rules('ct_alamat','Alamat','required');
			$this->form_validation->set_rules('ct_telepon','Telepon','required');

			if ($this->form_validation->run()==FALSE) {

				$id = $this->uri->segment(3);
				$data['row'] = $this->M_Pelanggan->get_pelanggan($id)->row();
				$this->template_admin->load('template_admin','pelanggan/ubah',$data);

			}
			else {

					$id['ct_id']			= $this->input->post('ct_id');
					$in['ct_nama'] 			= $this->input->post('ct_nama');
					$in['ct_alamat'] 		= $this->input->post('ct_alamat');
					$in['ct_telepon'] 		= $this->input->post('ct_telepon');						

					$this->db->update("tb_pelanggan",$in,$id);
							
					//$this->session->set_flashdata('simpan','Berhasil Di Simpan');
					$id = $this->input->post('ct_id');
					redirect('pelanggan/ubah/'.$id);
			}

		}

	
		else{
			redirect('auth');

		}
}	

	public function data()
	{
		$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
		$this->template_admin->load('template_admin','pelanggan/data',$data);

	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	Public function __construct(){
		parent:: __construct();
		$this->load->model('M_Auth');
	}

	public function index(){
		$data['tahun'] = $this->M_Auth->tahun();
		$this->load->view('login',$data);
	}


	/* function LoginUser(){
        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
 
        $cek_user=$this->M_Auth->login_user($username,$password);
 
        if($cek_user->num_rows() > 0){ 
                 $data=$cek_user->row_array();
                 $this->session->set_userdata('masuk',TRUE);
                if($data['level_id']=='1'){ //Akses admin
                    $this->session->set_userdata('level_id','1');
                    $this->session->set_userdata('id_user',$data['id_user']);
                    $this->session->set_userdata('username',$data['username']);
                    $this->session->set_userdata('password',$data['password']);
                    $this->session->set_flashdata("sukseslogin","Berhasil Login");
                    redirect('Admin');
 				}		
		}

 		$this->session->set_flashdata("gagallogin","Username / Password Salah");
		redirect('Login');

	} */

	function Login(){
        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

  /*       $log_status = 1;
        $cek_login=$this->M_Auth->cek_login($username,$log_status);
        if($cek_login->num_rows() > 0){ //jika user aktif
        $this->session->set_flashdata("logincek","Masih terdeteksi Login, silahkan hubungi admin ! ");         
        redirect('Login');
            
    }  */

        $cek_admin=$this->M_Auth->login_user($username,$password);
        
        if($cek_admin->num_rows() > 0){ 
                    $data=$cek_admin->row_array();
                    $this->session->set_userdata('masuk',TRUE);
                if($data['level_id']=='1'){ //Akses admin
                    $this->session->set_userdata('level_id',$data['level_id']);
                    $this->session->set_userdata('id_user',$data['id_user']);
                    $this->session->set_userdata('username',$data['username']);
                    $this->session->set_userdata('nama_user',$data['nama']);
                    $this->session->set_userdata('password',$data['password']);
                    $this->session->set_userdata('id_tahun',$this->input->post('id_tahun'));

                    $this->session->set_flashdata("sukseslogin","Berhasil Login");
                    redirect('Dashboard');
                }
                
                    $this->session->set_userdata('level_id',$data['level_id']);
                    $this->session->set_userdata('id_user',$data['id_user']);
                    $this->session->set_userdata('username',$data['username']);
                    $this->session->set_userdata('nama_user',$data['nama']);
                    $this->session->set_userdata('password',$data['password']);

                    $this->session->set_flashdata("sukseslogin","Berhasil Login");
                    //helper_log("login", "Login");
                    redirect('Dashboard');
            
                    }else{  // jika username dan password tidak ditemukan atau salah
                    $this->session->set_flashdata("gagallogin","Username atau Password Anda Salah!");
					redirect('Auth');
                    
        }
    }


	function logout() {
	/* 	$id = $this->session->userdata("username");
        $this->M_Auth->DeleteLog($id); */
		$this->session->sess_destroy();
		$this->session->set_flashdata("logout","Berhasil Logout");
		redirect("Auth");
		} 
	}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bahan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		$this->load->model('M_Bahan');
	}

	public function index()
	{
		$data['kode'] = $this->M_Bahan->kode_otomatis();
		$data['dataBahan'] = $this->M_Bahan->data_bahan();
		$this->template_admin->load('template_admin','pengeluaran/bahan/index',$data);

	}

	public function simpan() 
	{
		if($this->session->userdata("id_user")!=="" ) {

			$this->form_validation->set_rules('bh_kode','Kode','required');
			$this->form_validation->set_rules('bh_nama','Nama','required');
			$this->form_validation->set_rules('bh_jumlah','Jumlah','required');
			$this->form_validation->set_rules('bh_tanggal','Tanggal','required');
			$this->form_validation->set_rules('bh_harga','Harga','required');
			$this->form_validation->set_rules('bh_total_harga','Total Harga','required');
			
			if ($this->form_validation->run()==FALSE) {
				$data['kode'] = $this->M_Bahan->kode_otomatis();
				$this->template_admin->load('template_admin','pengeluaran/bahan/index',$data);
				$this->session->set_flashdata('datagagal',"Gagal Disimpan");
			}
			else {
					$today					= date("Y/m/d");
					//$id = $this->session->userdata("id_pegawai");
					//$id['id_pegawai']		= $this->input->post('id_pegawai');
					$in['bh_kode'] 			= $this->input->post('bh_kode');
					$in['bh_nama'] 			= $this->input->post('bh_nama');
					$in['bh_jumlah'] 		= $this->input->post('bh_jumlah');
					$in['bh_tanggal'] 		= $this->input->post('bh_tanggal');
					$in['bh_harga'] 		= $this->input->post('bh_harga');	
					$in['bh_total_harga'] 	= $this->input->post('bh_total_harga');	
					$in['bh_tanggal']		= $this->input->post('bh_tanggal');	

					$tr['tr_kode'] 			= $this->input->post('bh_kode');
					$tr['tr_nama']		 	= $this->input->post('bh_nama');
					$tr['tr_jenis'] 		= 2;
					$tr['tr_jumlah'] 		= $this->input->post('bh_jumlah');
					$tr['tr_harga'] 		= $this->input->post('bh_harga');
					$tr['tr_total_harga']	= $this->input->post('bh_total_harga');
					$tr['tr_tanggal']		= $this->input->post('bh_tanggal');	

					//$this->db->where("id_pegawai",$id);
					$this->db->insert("tb_bahan",$in);
					$this->db->insert("tb_transaksi",$tr);
					//helper_log("edit", "Mengubah Biodata");
					$this->session->set_flashdata('datasukses',"Data Berhasil Disimpan");
					
					redirect("bahan/index");
			}	

	}
		else{
			redirect('auth');

		}

	}

	public function data()
	{
		$data['dataBahan'] = $this->M_Bahan->data_bahan();
		$this->template_admin->load('template_admin','pengeluaran/bahan/data',$data);

	}

	public function delete() 
	{
		if($this->session->userdata("id_user")!=="" ) {
			$id = $this->uri->segment(3);
			$this->M_Bahan->delete_bahan($id);
			//$this->session->set_flashdata('hapus','Berhasil di Hapus !');
			redirect("bahan/data");
		}
		else{
			redirect('bahan');
		}
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		$this->load->helper('fungsi');
		$this->load->model('M_Bahan');
		$this->load->model('M_Count');
		$this->load->model('M_Transaksi');
	}

	public function index()
	{
		$data['kode'] = $this->M_Sablon->kode_otomatis();
		$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
		$this->template_admin->load('template_admin','pemasukan/sablon/index',$data);

	}

	public function data()
	{
		$data['dataBahan'] 	= $this->M_Bahan->data_bahan();
		$data['dataTransaksi'] 	= $this->M_Transaksi->data_transaksi_out();
		$x = $this->M_Count->chart_statistik_pengeluaran()->result();
		$data['chart2'] = json_encode($x);
		$this->template_admin->load('template_admin','pengeluaran/total/data',$data);

	}

}

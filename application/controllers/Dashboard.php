<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		//$this->load->model('M_Pelanggan');
		$this->load->model('M_Count');
	}

	public function index()
	{
        $data['sumBordir'] 				= $this->M_Count->sum_bordir();
        $data['sumSablon'] 				= $this->M_Count->sum_sablon();
		$data['sumSublime'] 			= $this->M_Count->sum_sublime();

		$data['sumBordirNot'] 			= $this->M_Count->sum_bordir_Not();
        $data['sumSablonNot'] 			= $this->M_Count->sum_sablon_Not();
		$data['sumSublimeNot'] 			= $this->M_Count->sum_sublime_Not();

		$data['sumBulanini'] 			= $this->M_Count->sum_bulan_ini();
		$data['sumTahunini'] 			= $this->M_Count->sum_tahun_ini();
		
		$x 								= $this->M_Count->chart_pemasukan()->result();
		$xx 							= $this->M_Count->chart_pengeluaran()->result();
		$xxx 							= $this->M_Count->chart_belum_lunas()->result();
		$data['chart6'] 				= $this->M_Count->chart_pemasukan_new()->result();
		$data['chart3'] 				= json_encode($x);
		$data['chart4'] 				= json_encode($xx);
		$data['chart5'] 				= json_encode($xxx);
		$data['dataTagihan'] 			= $this->M_Count->data_tagihan();
		$this->template_admin->load('template_admin','dashboard/index',$data);

	}

}

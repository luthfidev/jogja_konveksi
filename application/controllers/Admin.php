<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		$this->load->model('M_Admin');
		$this->load->model('M_Count');
	}

	public function index()
	{
		$data['sumBordir'] = $this->M_Count->sum_bordir();
        $data['sumSablon'] = $this->M_Count->sum_sablon();
		$data['sumSublime'] = $this->M_Count->sum_sublime();
		
		$this->template_admin->load('template_admin','dashboard/index',$data);

	}

	public function adduser()
	{
		$data['level'] = $this->M_Admin->dtlevel();
		$data['dataUser'] = $this->M_Admin->user();
		$this->template_admin->load('template_admin','Admin/user',$data);

	}

	 public function checkUsername()
	{
		  if($this->M_Admin->getUsername($_POST['username'])){
		   echo '<label class="text-danger"><span><i class="fa fa-times" aria-hidden="true">
		   </i> Username sudah digunakan</span></label>';
		  }
		  else {
		   echo '<label class="text-success"><span><i class="fa fa-check-circle-o" aria-hidden="true"></i> Username boleh digunakan</span></label>';
		  }
	 }

	public function simpanuser() 
	{
		if($this->session->userdata("id_user")!=="" ) {

			$this->form_validation->set_rules('level_id','Level','required');
			$this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('password','Password','required');
			
			if ($this->form_validation->run()==FALSE) {
				$data['level'] = $this->M_admin->user();
				$this->session->set_flashdata('datagagal',"Data Gagal Di Simpan");
				$this->template_admin->load('template_admin','admin/user',$data);
			}
			else {

					$user_exist = $in['username'] 		= $this->input->post('username');
					$in['password'] 		= MD5($this->input->post('password'));
					$in['level_id'] 		= $this->input->post('level_id');

					$sql = $this->db->query("SELECT username FROM tb_user where username='$user_exist'");
					$cek_user_exist = $sql->num_rows();
					if ($cek_user_exist > 0) {
					$this->session->set_flashdata('exist', 'Username Sudah Ada');
					redirect('Admin/adduser');
					}

					$this->db->insert("tb_user",$in);
					//helper_log("edit", "Mengubah Biodata");
					$this->session->set_flashdata('datasukses',"Data Berhasil Disimpan");
					
					redirect("admin/adduser");
			}
	}
		else{
			redirect('auth');

		}

	}

	public function deleteuser() 
	{
		if($this->session->userdata("id_user")!=="" ) {
			$id = $this->uri->segment(3);
			$this->M_Admin->delete_user($id);
			//$this->session->set_flashdata('hapus','Berhasil di Hapus !');
			redirect("admin/adduser");
		}
		else{
			redirect('admin/adduser');
		}
	}

}

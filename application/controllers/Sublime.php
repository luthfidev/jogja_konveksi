<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sublime extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		$this->load->model('M_Pelanggan');
		$this->load->model('M_Sublime');
	}

	public function index()
	{
		$data['kode'] = $this->M_Sublime->kode_otomatis();
		$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
		$data['dataSublime'] = $this->M_Sublime->data_sublime();
		$this->template_admin->load('template_admin','pemasukan/sublime/index',$data);
	}

	public function simpan() 
	{
		if($this->session->userdata("id_user")!=="" ) {

			$this->form_validation->set_rules('sbl_kode','Kode','required');
			$this->form_validation->set_rules('pelanggan_id_ct','Nama Pelanggan','required');
			$this->form_validation->set_rules('sbl_nama_pesanan','Nama Pesanan','required');
			$this->form_validation->set_rules('sbl_jumlah','Jumlah','required');
/* 			$this->form_validation->set_rules('sbl_stitch','Stitch','required');
			$this->form_validation->set_rules('sbl_total_stitch','Total Stitch','required'); */
			$this->form_validation->set_rules('sbl_datang','Datang','required');
			$this->form_validation->set_rules('sbl_kirim','Kirim','required');
			$this->form_validation->set_rules('sbl_harga','Harga','required');
			$this->form_validation->set_rules('sbl_total_harga','Total Harga','required');
			
			if ($this->form_validation->run()==FALSE) {
				$data['kode'] = $this->M_Sublime->kode_otomatis();
				$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
				$this->session->set_flashdata('datagagal',"Data Gagal Di Simpan");
				$this->template_admin->load('template_admin','pemasukan/sublime/index',$data);
			}
			else {
					$today					= date("Y/m/d");
					$year					= date("Y");
					//$id = $this->session->userdata("id_pegawai");
					//$id['id_pegawai']		= $this->input->post('id_pegawai');
					$in['sbl_kode'] 		= $this->input->post('sbl_kode');
					$in['pelanggan_id_ct'] 	= $this->input->post('pelanggan_id_ct');
					$in['sbl_nama_pesanan'] = $this->input->post('sbl_nama_pesanan');
					$in['sbl_jumlah'] 		= $this->input->post('sbl_jumlah');
					$in['sbl_datang'] 		= $this->input->post('sbl_datang');
					$in['sbl_kirim'] 		= $this->input->post('sbl_kirim');
					$in['sbl_harga'] 		= $this->input->post('sbl_harga');	
					$in['sbl_total_harga'] 	= $this->input->post('sbl_total_harga');
					$in['sbl_status'] 	= $this->input->post('sbl_status');
					$in['sbl_tanggal']		= $today;
					$in['sbl_tahun']		= $year;	

					$tr['tr_kode'] 			= $this->input->post('sbl_kode');
					$tr['pelanggan_id_ct'] 	= $this->input->post('pelanggan_id_ct');
					$tr['tr_nama'] 			= $this->input->post('sbl_nama_pesanan');
					$tr['tr_jenis'] 		= 1;
					$tr['tr_produk'] 		= "Sublime";
					$tr['tr_jumlah'] 		= $this->input->post('sbl_jumlah');
					$tr['tr_harga'] 		= $this->input->post('sbl_harga');
					$tr['tr_total_harga']	= $this->input->post('sbl_total_harga');
					$tr['tr_status'] 		= $this->input->post('sbl_status');	
					$tr['tr_tanggal']		= $today;
					$tr['tr_tahun']			= $year;

					//$this->db->where("id_pegawai",$id);
					$this->db->insert("tb_sublime",$in);
					$this->db->insert("tb_transaksi",$tr);
					//helper_log("edit", "Mengubah Biodata");
					$this->session->set_flashdata('datasukses',"Data Berhasil Disimpan");
					
					redirect("sublime/index");
			}

	}
		else{
			redirect('auth');

		}

	}

	public function delete() 
	{
		if($this->session->userdata("id_user")!=="" ) {
			$id = $this->uri->segment(3);
			$this->M_Sublime->delete_sublime($id);
			//$this->session->set_flashdata('hapus','Berhasil di Hapus !');
			redirect("sublime/data");
		}
		else{
			redirect('sublime');
		}
	}

	public function switch(){
		$id = $this->uri->segment(3);
		$kat = $this->uri->segment(4);
		if($kat == "2"){
			$this->db->query("update tb_sublime set sbl_status = 'Lunas' where sbl_id = '$id'");
			redirect('sublime/data');
		}elseif($kat == "1"){
			$this->db->query("update tb_sublime set sbl_status = 'Belum Lunas' where sbl_id = '$id'");
			redirect('sublime/data');
		}
	}

	public function data()
	{
		$data['dataSublime'] = $this->M_Sublime->data_sublime();
		$this->template_admin->load('template_admin','pemasukan/sublime/data',$data);

	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sablon extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		$this->load->helper('fungsi');
		$this->load->model('M_Pelanggan');
		$this->load->model('M_Sablon');
	}

	public function index()
	{
		$data['kode'] = $this->M_Sablon->kode_otomatis();
		$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
		$data['dataSablon'] = $this->M_Sablon->data_sablon();
		$this->template_admin->load('template_admin','pemasukan/sablon/index',$data);

	}

	public function simpan() 
	{
		if($this->session->userdata("id_user")!=="" ) {

			$this->form_validation->set_rules('sb_kode','Kode','required');
			$this->form_validation->set_rules('pelanggan_id_ct','Nama Pelanggan','required');
			$this->form_validation->set_rules('sb_nama_pesanan','Nama Pesanan','required');
			$this->form_validation->set_rules('sb_jumlah','Jumlah','required');
/* 			$this->form_validation->set_rules('sb_stitch','Stitch','required');
			$this->form_validation->set_rules('sb_total_stitch','Total Stitch','required'); */
			$this->form_validation->set_rules('sb_datang','Datang','required');
			$this->form_validation->set_rules('sb_kirim','Kirim','required');
			$this->form_validation->set_rules('sb_harga','Harga','required');
			$this->form_validation->set_rules('sb_total_harga','Total Harga','required');
			
			if ($this->form_validation->run()==FALSE) {
				$data['kode'] = $this->M_Sablon->kode_otomatis();
				$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
				$this->session->set_flashdata('datagagal',"Data Gagal Di Simpan");
				$this->template_admin->load('template_admin','pemasukan/sablon/index',$data);
			}
			else {
					$today					= date("Y/m/d");
					$year					= date("Y");
					//$id = $this->session->userdata("id_pegawai");
					//$id['id_pegawai']		= $this->input->post('id_pegawai');
					$in['sb_kode'] 			= $this->input->post('sb_kode');
					$in['pelanggan_id_ct'] 	= $this->input->post('pelanggan_id_ct');
					$in['sb_nama_pesanan'] 	= $this->input->post('sb_nama_pesanan');
					$in['sb_jumlah'] 		= $this->input->post('sb_jumlah');
					/* $in['sb_stitch'] 		= $this->input->post('sb_stitch');
					$in['sb_total_stitch'] 	= $this->input->post('sb_total_stitch'); */
					$in['sb_datang'] 		= $this->input->post('sb_datang');
					$in['sb_kirim'] 		= $this->input->post('sb_kirim');
					$in['sb_harga'] 		= $this->input->post('sb_harga');	
					$in['sb_total_harga'] 	= $this->input->post('sb_total_harga');
					$in['sb_status'] 		= $this->input->post('sb_status');	
					$in['sb_tanggal']		= $today;
					$in['sb_tahun']			= $year;

					$tr['tr_kode'] 			= $this->input->post('sb_kode');
					$tr['pelanggan_id_ct'] 	= $this->input->post('pelanggan_id_ct');
					$tr['tr_nama'] 			= $this->input->post('sb_nama_pesanan');
					$tr['tr_jenis'] 		= 1;
					$tr['tr_produk'] 		= "Sablon";
					$tr['tr_jumlah'] 		= $this->input->post('sb_jumlah');
					$tr['tr_harga'] 		= $this->input->post('sb_harga');
					$tr['tr_total_harga']	= $this->input->post('sb_total_harga');
					$tr['tr_status'] 		= $this->input->post('sb_status');	
					$tr['tr_tanggal']		= $today;
					$tr['tr_tahun']			= $year;

					//$this->db->where("id_pegawai",$id);
					$this->db->insert("tb_sablon",$in);
					$this->db->insert("tb_transaksi",$tr);
					//helper_log("edit", "Mengubah Biodata");
					$this->session->set_flashdata('datasukses',"Data Berhasil Disimpan");
					
					redirect("sablon/index");
			}	

	}
		else{
			redirect('auth');

		}

	}

	public function delete() 
	{
		if($this->session->userdata("id_user")!=="" ) {
			$id = $this->uri->segment(3);
			$this->M_Sablon->delete_sablon($id);
			//$this->session->set_flashdata('hapus','Berhasil di Hapus !');
			redirect("sablon/data");
		}
		else{
			redirect('sablon');
		}
	}

	public function switch(){
		$id = $this->uri->segment(3);
		$kat = $this->uri->segment(4);
		if($kat == "2"){
			$this->db->query("update tb_sablon set sb_status = 'Lunas' where sb_id = '$id'");
			redirect('sablon/index');
		}elseif($kat == "1"){
			$this->db->query("update tb_sablon set sb_status = 'Belum Lunas' where sb_id = '$id'");
			redirect('sablon/index');
		}
	}

	public function data()
	{
		$data['dataSablon'] = $this->M_Sablon->data_sablon();
		$this->template_admin->load('template_admin','pemasukan/sablon/data',$data);

	}

}

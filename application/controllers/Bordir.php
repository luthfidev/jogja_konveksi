<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Bordir extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		$this->load->model('M_Pelanggan');
		$this->load->model('M_Bordir');
	}

	public function index()
	{
		$data['kode'] = $this->M_Bordir->kode_otomatis();
		$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
		$data['dataBordir'] = $this->M_Bordir->data_bordir();
		$this->template_admin->load('template_admin','pemasukan/bordir/index',$data);
	}

	public function simpan() 
	{
		if($this->session->userdata("id_user")!=="" ) {

			$this->form_validation->set_rules('br_kode','Kode','required');
			$this->form_validation->set_rules('pelanggan_id_ct','Nama Pelanggan','required');
			$this->form_validation->set_rules('br_nama_pesanan','Nama Pesanan','required');
			$this->form_validation->set_rules('br_jumlah','Jumlah','required');
			$this->form_validation->set_rules('br_stitch','Stitch','required');
			$this->form_validation->set_rules('br_total_stitch','Total Stitch','required');
			$this->form_validation->set_rules('br_datang','Datang','required');
			$this->form_validation->set_rules('br_kirim','Kirim','required');
			$this->form_validation->set_rules('br_harga','Harga','required');
			$this->form_validation->set_rules('br_total_harga','Total Harga','required');
			
			if ($this->form_validation->run()==FALSE) {
				$data['kode'] = $this->M_Bordir->kode_otomatis();
				$data['pelanggan'] = $this->M_Pelanggan->data_pelanggan();
				$this->session->set_flashdata('datagagal',"Data Gagal Di Simpan");
				$this->template_admin->load('template_admin','pemasukan/bordir/index',$data);
			}
			else {
					$today					= date("Y/m/d");
					$year					= date("Y");
					//$id = $this->session->userdata("id_pegawai");
					//$id['id_pegawai']		= $this->input->post('id_pegawai');
					$in['br_kode'] 			= $this->input->post('br_kode');
					$in['pelanggan_id_ct'] 	= $this->input->post('pelanggan_id_ct');
					$in['br_nama_pesanan'] 	= $this->input->post('br_nama_pesanan');
					$in['br_jumlah'] 		= $this->input->post('br_jumlah');
					$in['br_stitch'] 		= $this->input->post('br_stitch');
					$in['br_total_stitch'] 	= $this->input->post('br_total_stitch');
					$in['br_datang'] 		= $this->input->post('br_datang');
					$in['br_kirim'] 		= $this->input->post('br_kirim');
					$in['br_harga'] 		= $this->input->post('br_harga');	
					$in['br_total_harga'] 	= $this->input->post('br_total_harga');	
					$in['br_status'] 		= $this->input->post('br_status');	
					$in['br_tanggal']		= $today;
					$in['br_tahun']			= $year;

					$tr['tr_kode'] 			= $this->input->post('br_kode');
					$tr['pelanggan_id_ct'] 	= $this->input->post('pelanggan_id_ct');
					$tr['tr_nama']		 	= $this->input->post('br_nama_pesanan');
					$tr['tr_jenis'] 		= 1;
					$tr['tr_produk'] 		= "Bordir";
					$tr['tr_status'] 		= $this->input->post('br_status');	
					$tr['tr_stitch'] 		= $this->input->post('br_stitch');
					$tr['tr_total_stitch'] 	= $this->input->post('br_total_stitch');
					$tr['tr_jumlah'] 		= $this->input->post('br_jumlah');
					$tr['tr_harga'] 		= $this->input->post('br_harga');
					$tr['tr_total_harga']	= $this->input->post('br_total_harga');
					$tr['tr_tanggal']		= $today;
					$tr['tr_tahun']			= $year;
					$tr['tr_datang']		= $this->input->post('br_datang');
					$tr['tr_kirim']			= $this->input->post('br_kirim');
					//$this->db->where("id_pegawai",$id);
					$this->db->insert("tb_bordir",$in);
					$this->db->insert("tb_transaksi",$tr);
					//helper_log("edit", "Mengubah Biodata");
					$this->session->set_flashdata('datasukses',"Data Berhasil Disimpan");
					
					redirect("bordir/index");
			}		

	}
		else{
			redirect('auth');

		}
	}

	public function data()
	{
		$data['dataBordir'] = $this->M_Bordir->data_bordir();
		$this->template_admin->load('template_admin','pemasukan/bordir/data',$data);

	}

	public function switch()
	{
		$id = $this->uri->segment(3);
		$kat = $this->uri->segment(4);
		if($kat == "2"){
			$this->db->query("update tb_bordir set br_status = 'Lunas' where br_id = '$id'");
			redirect('bordir/data');
		}elseif($kat == "1"){
			$this->db->query("update tb_bordir set br_status = 'Belum Lunas' where br_id = '$id'");
			redirect('bordir/data');
		}
	}

	public function delete() 
	{
		if($this->session->userdata("id_user")!=="" ) {
			$id = $this->uri->segment(3);
			$this->M_Bordir->delete_bordir($id);
			//$this->session->set_flashdata('hapus','Berhasil di Hapus !');
			redirect("bordir/data");
		}
		else{
			redirect('bordir');
		}
	}

}

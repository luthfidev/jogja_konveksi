<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*	if($this->session->userdata('masuk')==false){
			redirect('login');
		}*/
		$this->load->helper('fungsi');
		$this->load->model('M_Transaksi');
		$this->load->model('M_Laporan');
	}

	public function index()
	{
		$this->template_admin->load('template_admin','laporan/index');

	}

	public function keuangan()
	{
		$data['dataKeuangan'] = $this->M_Laporan->keuangan();
		$this->template_admin->load('template_admin','laporan/keuangan/index',$data);
	}

	public function laporanHarianIn()
	{
		$data['harianIn'] = $this->M_Laporan->laporan_harian_in();
		$this->template_admin->load('template_admin','laporan/harian/index',$data);
	}

	public function laporanBulananIn()
	{
		$data['bulananIn'] = $this->M_Laporan->laporan_bulanan_in();
		$this->template_admin->load('template_admin','laporan/bulanan/index',$data);
	}

	public function cariLaporan()
	{
		
		$jenis			= $this->input->post('jenis');
		$status			= $this->input->post('status');
		$jenisproduk	= $this->input->post('jenis_produk');
		$tahun			= $this->input->post('tahun');
		$startdate		= $this->input->post('start_date');	
		$enddate		= $this->input->post('end_date');
		$data['dataKeuangan'] =	$this->M_Laporan->get_laporan($jenis,$status,$jenisproduk,$tahun,$startdate,$enddate);
		$this->template_admin->load('template_admin','laporan/hasilcari',$data);
		//$this->load->view("laporan/hasilcari",$data);
	}

	public function cetakLaporan()
	{
		
		//$data['row'] = $this->M_Invoice->get_invoice($id)->row();
		//$this->template_admin->load('template_admin','cetak_penawaran/penawaran',$data);
		$jenis			= $this->input->post('jenis');
		$status			= $this->input->post('status');
		$jenisproduk	= $this->input->post('jenis_produk');
		$tahun			= $this->input->post('tahun');
		$startdate		= $this->input->post('start_date');	
		$enddate		= $this->input->post('end_date');
		$data['dataKeuangan'] =	$this->M_Laporan->get_laporan($jenis,$status,$jenisproduk,$tahun,$startdate,$enddate);
		//$html = $this->template_admin->load('template_admin','laporan/hasilcari',$data, TRUE);
		$html = $this->load->view('laporan/hasilcari',$data,TRUE);
		// $filename = 'report_'.time();
		 /*$this->pdfgenerator->generate($html,'cetak','A4','portrait');*/
		 $this->pdfgenerator->generate($html,'cetakk','A4','landscape');
	}

	public function cetakLaporanBordir()
	{

		$dompdf = new Dompdf\Dompdf();
		//$data['row'] = $this->M_Invoice->get_invoice($id)->row();
		//$this->template_admin->load('template_admin','cetak_penawaran/penawaran',$data);
		$jenis			= $this->input->post('jenis');
		$status			= $this->input->post('status');
		$jenisproduk	= $this->input->post('jenis_produk');
		$tahun			= $this->input->post('tahun');
		$startdate		= $this->input->post('start_date');	
		$enddate		= $this->input->post('end_date');
		$data['dataKeuangan'] =	$this->M_Laporan->get_laporan($jenis,$status,$jenisproduk,$tahun,$startdate,$enddate);
		//$html = $this->template_admin->load('template_admin','laporan/hasilcari',$data, TRUE);
		$html = $this->load->view('laporan/hasilcaribordir',$data,TRUE);
		// $filename = 'report_'.time();
		 /*$this->pdfgenerator->generate($html,'cetak','A4','portrait');*/
		// $this->pdfgenerator->generate($html,'cetakk','A4','landscape');
		 $dompdf->loadHtml($html);
		 $dompdf->setPaper(array(0,0,609.4488,935.433), 'landscape');
		 $dompdf->render();
		 $pdf = $dompdf->output();
		 $filename = 'report_'.time();
		 $dompdf->stream($filename, array('Attachment' => 0));
	}

	
}

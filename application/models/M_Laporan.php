<?php
class M_Laporan extends CI_Model{



    function keuangan()
	{
        $Dnow = date("Y-m-d");
        $this->db->select('*');
        //$this->db->select('tr_total_harga');
       // $this->db->group_by('DATE((tr_tanggal)');
        // /$this->db->where('tr_tanggal',$Dnow); 
      // $this->db->where('tr_jenis',1);
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

    function laporan_harian_in()
	{
        $Dnow = date("Y-m-d");
        $this->db->select('*');
        //$this->db->select('tr_total_harga');
       // $this->db->group_by('DATE((tr_tanggal)');
        $this->db->where('tr_tanggal',$Dnow); 
      // $this->db->where('tr_jenis',1);
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

    function laporan_bulanan_in()
	{
        $DMonth = date("Y-m");
        $this->db->select('*');
        //$this->db->select('tr_total_harga');
       // $this->db->group_by('YEAR(tr_tanggal)');
        $this->db->where('tr_tanggal',$DMonth);
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

    function laporan_tahunan_in()
	{
        $this->db->select('YEAR(tr_tanggal), SUM(tr_total_harga) as total, tr_jenis');
        //$this->db->select('tr_total_harga');
        $this->db->group_by('YEAR(tr_tanggal)');
        $this->db->group_by('tr_jenis');
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

     function get_laporan($jenis,$status,$jenisproduk,$tahun,$startdate,$enddate)
    {
           $this->db->select('*');
           //$this->db->select_sum('tr_total_harga','total', FALSE);
          //  $this->db->from('tb_transaksi');
            $this->db->where('tr_jenis',$jenis);
            $this->db->where('tr_status',$status);
            $this->db->where('tr_produk',$jenisproduk);
            $this->db->where('tr_tahun',$tahun);
            $this->db->where('tr_tanggal >=',$startdate);
            $this->db->where('tr_tanggal <=',$enddate);
            $query = $this->db->get('tb_transaksi');
            return $query;        
    }


}
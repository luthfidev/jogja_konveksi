<?php
class M_Sublime extends CI_Model{


	function kode_otomatis()
	{
		$this->db->select('RIGHT(tb_sublime.sbl_kode,2) as kode_sbl', FALSE);
		$this->db->order_by('kode_sbl','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tb_sublime');  //cek dulu apakah ada sudah ada kode di tabel.    
		if($query->num_rows() <> 0){      
			 //cek kode jika telah tersedia    
			 $data = $query->row();      
			 $kode = intval($data->kode_sbl) + 1; 
		}
		else{      
			 $kode = 1;  //cek jika kode belum terdapat pada table
		}
			$tgl=date('dmY'); 
			$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			$kodetampil = "SBL"."5".$tgl.$batas;  //format kode
			return $kodetampil;  
	}

	function data_sublime()
	{
		$tahun = $this->session->userdata('id_tahun');
        $this->db->select('*');
        $this->db->from('tb_sublime');
        $this->db->join('tb_pelanggan','tb_pelanggan.ct_id = tb_sublime.pelanggan_id_ct');
		$this->db->order_by('sbl_id','DESC');
		$this->db->where('YEAR(sbl_tanggal)',$tahun);
        $query = $this->db->get();
        return $query;
	}
	
	function delete_sublime($id) 
	{
		return $this->db->query("delete from tb_sublime where sbl_id='$id' ");
	}


}
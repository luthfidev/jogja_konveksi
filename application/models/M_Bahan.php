<?php
class M_Bahan extends CI_Model{


	function kode_otomatis()
	{
		$this->db->select('RIGHT(tb_bahan.bh_kode,2) as kode_bh', FALSE);
		$this->db->order_by('kode_bh','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tb_bahan');  //cek dulu apakah ada sudah ada kode di tabel.    
		if($query->num_rows() <> 0){      
			 //cek kode jika telah tersedia    
			 $data = $query->row();      
			 $kode = intval($data->kode_bh) + 1; 
		}
		else{      
			 $kode = 1;  //cek jika kode belum terdapat pada table
		}
			$tgl=date('dmY'); 
			$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			$kodetampil = "BH"."5".$tgl.$batas;  //format kode
			return $kodetampil;  
	}

	function data_bahan()
	{
		$tahun = $this->session->userdata('id_tahun');
		$this->db->select('*');
        $this->db->from('tb_bahan');
        //$this->db->join('tb_pelanggan','tb_pelanggan.ct_id = tb_sablon.pelanggan_id_ct');
		$this->db->order_by('bh_id','DESC');
		$this->db->where('YEAR(bh_tanggal)',$tahun);
        $query = $this->db->get();
        return $query;
	}
	
	function delete_bahan($id) 
	{
		return $this->db->query("delete from tb_bahan where bh_id='$id' ");
	}


}
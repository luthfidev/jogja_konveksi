<?php
class M_Admin extends CI_Model{


	function user()
	{
		$this->db->select('*');
        $this->db->from('tb_user');
        $this->db->join('tb_level','tb_level.id_level = tb_user.level_id');
		$this->db->order_by('id_user','DESC');
		//$this->db->where('YEAR(sb_tanggal)',$tahun);
        $query = $this->db->get();
        return $query;
	}

	 function getUsername($username)
	 {
		  $this->db->where('username' , $username);
		  $query = $this->db->get('tb_user');

		  if($query->num_rows()>0){
		   return true;
		  }
		  else {
		   return false;
		  }
	 }

	function dtlevel()
	{
		$this->db->select('*');
        $this->db->from('tb_level');
		$this->db->order_by('id_level','DESC');
		//$this->db->where('YEAR(sb_tanggal)',$tahun);
        $query = $this->db->get();
        return $query;
	}
	
	function delete_user($id) 
	{
		return $this->db->query("delete from tb_user where id_user='$id' ");
	}


}
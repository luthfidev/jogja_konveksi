<?php
class M_Pelanggan extends CI_Model{


	function kode_otomatis()
	{
		$this->db->select('RIGHT(tb_pelanggan.ct_kode,2) as kode_ct', FALSE);
		$this->db->order_by('kode_ct','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tb_pelanggan');  //cek dulu apakah ada sudah ada kode di tabel.    
		if($query->num_rows() <> 0){      
			 //cek kode jika telah tersedia    
			 $data = $query->row();      
			 $kode = intval($data->kode_ct) + 1; 
		}
		else{      
			 $kode = 1;  //cek jika kode belum terdapat pada table
		}
			$tgl=date('Y'); 
			$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			//$kodetampil = "CT"."5".$tgl.$batas;  //format kode
			$kodetampil = "C".$tgl.$batas;
			return $kodetampil;  
	}

	function get_pelanggan($id)
	{
		$this->db->select('*');
		$this->db->from('tb_pelanggan');
		$this->db->where('ct_id',$id);
		$query = $this->db->get();
		return $query;
	}

	function data_pelanggan()
	{
		$this->db->select("*");
		$this->db->order_by("ct_created_time", "DESC");
		return $this->db->get("tb_pelanggan");
	}


}
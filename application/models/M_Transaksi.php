<?php
class M_Transaksi extends CI_Model{

/* 
	function kode_otomatis()
	{
		$this->db->select('RIGHT(tb_pelanggan.ct_kode,2) as kode_ct', FALSE);
		$this->db->order_by('kode_ct','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tb_pelanggan');  //cek dulu apakah ada sudah ada kode di tabel.    
		if($query->num_rows() <> 0){      
			 //cek kode jika telah tersedia    
			 $data = $query->row();      
			 $kode = intval($data->kode_ct) + 1; 
		}
		else{      
			 $kode = 1;  //cek jika kode belum terdapat pada table
		}
			$tgl=date('dmY'); 
			$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			$kodetampil = "CT"."5".$tgl.$batas;  //format kode
			return $kodetampil;  
	} */



	function data_transaksi()
	{
		$this->db->select("*");
		$this->db->from('tb_transaksi');
		$this->db->join('tb_pelanggan','tb_pelanggan.ct_id = tb_transaksi.pelanggan_id_ct');
		$this->db->order_by("tr_kode", "DESC");
		return $this->db->get();
    }

	function data_transaksi_in()
	{

		$tahun = $this->session->userdata('id_tahun');
		$this->db->select('*');
		$this->db->from('tb_transaksi');
		$this->db->join('tb_pelanggan','tb_pelanggan.ct_id = tb_transaksi.pelanggan_id_ct');
		$this->db->order_by('tr_id','DESC');
		$this->db->where('tr_jenis',1);
	    $this->db->where('YEAR(tr_tanggal)',$tahun);
	    $this->db->group_by('ct_nama');
        $query = $this->db->get();
        return $query;
    }

	function data_transaksi_out()
	{
		$tahun = $this->session->userdata('id_tahun');
		$this->db->select("*");
		$this->db->order_by("tr_kode", "DESC");
		$this->db->where("tr_jenis",2);
		$this->db->where('YEAR(tr_tanggal)',$tahun);
		return $this->db->get("tb_transaksi");
    }

 
    


}
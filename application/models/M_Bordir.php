<?php
class M_Bordir extends CI_Model{


	function kode_otomatis()
	{
		$this->db->select('RIGHT(tb_bordir.br_kode,2) as kode_br', FALSE);
		$this->db->order_by('kode_br','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tb_bordir');  //cek dulu apakah ada sudah ada kode di tabel.    
		if($query->num_rows() <> 0){      
			 //cek kode jika telah tersedia    
            $data = $query->row();      
            $kode = intval($data->kode_br) + 1; 
		}
		else{      
			 $kode = 1;  //cek jika kode belum terdapat pada table
		}
			$tgl=date('dmY'); 
			$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			$kodetampil = "BR"."5".$tgl.$batas;  //format kode
			return $kodetampil;  
	}

	function data_bordir()
	{
		$tahun = $this->session->userdata('id_tahun');
		$this->db->select('*');
        $this->db->from('tb_bordir');
        $this->db->join('tb_pelanggan','tb_pelanggan.ct_id = tb_bordir.pelanggan_id_ct');
		$this->db->order_by('br_id','DESC');
		$this->db->where('YEAR(br_tanggal)',$tahun);
        $query = $this->db->get();
        return $query;
	}
	
	function delete_bordir($id) 
	{
		return $this->db->query("delete from tb_bordir where br_id='$id' ");
	}


}
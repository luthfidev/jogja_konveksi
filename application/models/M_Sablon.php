<?php
class M_Sablon extends CI_Model{


	function kode_otomatis()
	{
		$this->db->select('RIGHT(tb_sablon.sb_kode,2) as kode_sb', FALSE);
		$this->db->order_by('kode_sb','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tb_sablon');  //cek dulu apakah ada sudah ada kode di tabel.    
		if($query->num_rows() <> 0){      
			 //cek kode jika telah tersedia    
			 $data = $query->row();      
			 $kode = intval($data->kode_sb) + 1; 
		}
		else{      
			 $kode = 1;  //cek jika kode belum terdapat pada table
		}
			$tgl=date('dmY'); 
			$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			$kodetampil = "SB"."5".$tgl.$batas;  //format kode
			return $kodetampil;  
	}

	function data_sablon()
	{
		$tahun = $this->session->userdata('id_tahun');
		$kode = "SB";
		$this->db->select('*');
        $this->db->from('tb_sablon');
        $this->db->join('tb_pelanggan','tb_pelanggan.ct_id = tb_sablon.pelanggan_id_ct');
		$this->db->order_by('sb_id','DESC');
		//$this->db->where('YEAR(sb_tanggal)',$tahun);
		$this->db->where('LEFT(sb_kode,"2")',$kode);
        $query = $this->db->get();
        return $query;
	}
	
	function delete_sablon($id) 
	{
		return $this->db->query("delete from tb_sablon where sb_id='$id' ");
	}


}
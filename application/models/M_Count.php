<?php
class M_Count extends CI_Model{

    
	
    function sum_bordir()
	{
        $tahun = $this->session->userdata('id_tahun');
        $this->db->select_sum('br_total_harga');
        $this->db->where('YEAR(br_tanggal)',$tahun);
        $this->db->where('br_status',"Lunas");
        $query = $this->db->get('tb_bordir');
        if($query->num_rows()>0)
        {
            return $query->row()->br_total_harga;
        }
        else
        {
            return 0;
        }
    }

    function sum_sablon()
	{
        $tahun = $this->session->userdata('id_tahun');
        $this->db->select_sum('sb_total_harga');
        $this->db->where('YEAR(sb_tanggal)',$tahun);
        $this->db->where('sb_status',"Lunas");
        $query = $this->db->get('tb_sablon');
        if($query->num_rows()>0)
        {
            return $query->row()->sb_total_harga;
        }
        else
        {
            return 0;
        }
    }

    function sum_sublime()
	{
        $tahun = $this->session->userdata('id_tahun');
        $this->db->select_sum('sbl_total_harga');
        $this->db->where('YEAR(sbl_tanggal)',$tahun);
        $this->db->where('sbl_status',"Lunas");
        $query = $this->db->get('tb_sublime');
        if($query->num_rows()>0)
        {
            return $query->row()->sbl_total_harga;
        }
        else
        {
            return 0;
        }
    }

     function sum_bordir_Not()
    {
        $tahun = $this->session->userdata('id_tahun');
        $this->db->select_sum('br_total_harga');
        $this->db->where('YEAR(br_tanggal)',$tahun);
        $this->db->where('br_status',"Belum Lunas");
        $query = $this->db->get('tb_bordir');
        if($query->num_rows()>0)
        {
            return $query->row()->br_total_harga;
        }
        else
        {
            return 0;
        }
    }

       function sum_sablon_Not()
    {
        $tahun = $this->session->userdata('id_tahun');
        $this->db->select_sum('sb_total_harga');
        $this->db->where('YEAR(sb_tanggal)',$tahun);
        $this->db->where('sb_status',"Belum Lunas");
        $query = $this->db->get('tb_sablon');
        if($query->num_rows()>0)
        {
            return $query->row()->sb_total_harga;
        }
        else
        {
            return 0;
        }
    }

     function sum_sublime_Not()
    {
        $tahun = $this->session->userdata('id_tahun');
        $this->db->select_sum('sbl_total_harga');
        $this->db->where('YEAR(sbl_tanggal)',$tahun);
        $this->db->where('sbl_status',"Belum Lunas");
        $query = $this->db->get('tb_sublime');
        if($query->num_rows()>0)
        {
            return $query->row()->sbl_total_harga;
        }
        else
        {
            return 0;
        }
    }

      function sum_bulan_ini()
    {
        $tahun = $this->session->userdata('id_tahun');
       $this->db->select('date_format(tr_tanggal, "%m") as bulan, SUM(tr_total_harga) as total');
        //$this->db->select('tr_total_harga');
    //  $this->db->group_by('MONTH(tr_tanggal)');
       $this->db->where('YEAR(tr_tanggal)',$tahun);
        $this->db->group_by('bulan');      
       $this->db->where('tr_jenis',1);
      // $this->db->where('tr_status',"Lunas");
     //   $this->db->where('tr_jenis',2);
        $query = $this->db->get('tb_transaksi');
       // return $query->row()->total;
         if($query->num_rows()>0)
        {
            return $query->row()->total;
        }
        else
        {
            return 0;
        }
    }

      function sum_tahun_ini()
    {
          $tahun = $this->session->userdata('id_tahun');
       $this->db->select('date_format(tr_tanggal, "%Y") as tahun, SUM(tr_total_harga) as total');
        //$this->db->select('tr_total_harga');
    //  $this->db->group_by('MONTH(tr_tanggal)');
       $this->db->where('YEAR(tr_tanggal)',$tahun);
        $this->db->group_by('tahun');      
       $this->db->where('tr_jenis',1);
      // $this->db->where('tr_status',"Lunas");
     //   $this->db->where('tr_jenis',2);
        $query = $this->db->get('tb_transaksi');
       // return $query->row()->total;
         if($query->num_rows()>0)
        {
            return $query->row()->total;
        }
        else
        {
            return 0;
        }
    }

 //    function chart_statistik()
	// {
 //        $this->db->select('date_format(tr_tanggal, "%Y-%m") as bulan, SUM(tr_total_harga) as total');
 //        //$this->db->select('tr_total_harga');
 //        $this->db->group_by('bulan');
 //        $this->db->where('tr_jenis',1);
 //        $this->db->where('tr_status',"Lunas");
 //        $query = $this->db->get('tb_transaksi');
 //        return $query;
 //    }

    function chart_statistik_pengeluaran()
	{
        $this->db->select('YEAR(tr_tanggal), SUM(tr_total_harga) as total');
        //$this->db->select('tr_total_harga');
        $this->db->group_by('YEAR(tr_tanggal)');
        $this->db->where('tr_jenis',2);
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

 /*   function chart_pemasukan_pengeluaran()
	{
        $this->db->select('date_format(tr_tanggal, "%M") as bulan, SUM(tr_total_harga) as total, tr_jenis');
        //$this->db->select('tr_total_harga');
      $this->db->group_by('MONTH(tr_tanggal)');
       // $this->db->where('tr_jenis',1);
     //   $this->db->where('tr_jenis',2);
        $query = $this->db->get('tb_transaksi');
        return $query;
    }*/

     function chart_statistik_tahun()
    {
        $this->db->select('date_format(tr_tanggal, "%Y") as tahun, SUM(tr_total_harga) as total');
        $this->db->group_by('tahun'); 
        $this->db->where('tr_jenis',1);
        $this->db->where('tr_status',"Lunas");
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

     function chart_pemasukan()
    {
        $this->db->select('date_format(tr_tanggal, "%Y-%m") as bulan, SUM(tr_total_harga) as total');
        //$this->db->select('tr_total_harga');
    //  $this->db->group_by('MONTH(tr_tanggal)');
        $this->db->group_by('bulan');      
       $this->db->where('tr_jenis',1);
       $this->db->where('tr_status',"Lunas");
     //   $this->db->where('tr_jenis',2);
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

      function chart_pemasukan_new()
    {
        $this->db->select('date_format(tr_tanggal, "%Y-%m") as bulan,tr_produk, SUM(tr_total_harga) as total');
        //$this->db->select('tr_total_harga');
    //  $this->db->group_by('MONTH(tr_tanggal)');
       $this->db->group_by('tr_produk'); 
       // $this->db->group_by('bulan');     
        $this->db->where('tr_jenis',1);
       //$this->db->where('tr_produk',"Sublime");
        $this->db->where('tr_status',"Lunas");
     //   $this->db->where('tr_jenis',2);
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

     function chart_belum_lunas()
    {
        $this->db->select('date_format(tr_tanggal, "%Y-%m") as bulan, SUM(tr_total_harga) as total');
        //$this->db->select('tr_total_harga');
        //$this->db->group_by('MONTH(tr_tanggal)');
        $this->db->group_by('bulan');   
       $this->db->where('tr_jenis',1);
       $this->db->where('tr_status',"Belum Lunas");
     //   $this->db->where('tr_jenis',2);
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

      function chart_pengeluaran()
    {
        $this->db->select('date_format(tr_tanggal, "%Y-%m") as bulan, SUM(tr_total_harga) as total');
        //$this->db->select('tr_total_harga');
        //$this->db->group_by('MONTH(tr_tanggal)')
        $this->db->group_by('bulan');   ;
       $this->db->where('tr_jenis',2);
     //   $this->db->where('tr_jenis',2);
        $query = $this->db->get('tb_transaksi');
        return $query;
    }

       function data_tagihan()
    {
        $this->db->select("*");
        $this->db->from('tb_transaksi');
        $this->db->join('tb_pelanggan','tb_pelanggan.ct_id = tb_transaksi.pelanggan_id_ct');
        $this->db->order_by("tr_kode", "DESC");
        $this->db->where('tr_status',"Belum Lunas");
        return $this->db->get();
    }

}
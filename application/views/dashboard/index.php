
<div class="section-header">
    <h1>Dashboard | Tahun <?php echo $this->session->userdata('id_tahun'); ?></h1>

</div>

<div class="section-body">
<div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
                <i class="fab fa-steam-symbol"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Pemasukan Bordir</h4>
                </div>
                <div class="card-body">
                Rp.<?php echo rupiah($sumBordir); ?>
                </div>
            </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
                <i class="fas fa-paint-brush"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Pemasukan Sablon</h4>
                </div>
                <div class="card-body">
                Rp.<?php echo rupiah($sumSablon); ?>
                </div>
            </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
                <i class="fas fa-tint"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Pemasukan Sublime</h4>
                </div>
                <div class="card-body">
                Rp.<?php echo rupiah($sumSublime); ?>
                </div>
            </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-success">
                <i class="fas fa-chart-line"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Total Pemasukan</h4>
                </div>
                <div class="card-body">
                <?php 
                
                $ttlSSB = $sumBordir + $sumSablon +$sumSublime;

                echo rupiah($ttlSSB);
                
                ?>
                </div>
            </div>
            </div>
        </div>
         <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
                <i class="fab fa-steam-symbol"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Belum Bayar Bordir</h4>
                </div>
                <div class="card-body">
                Rp.<?php echo rupiah($sumBordirNot); ?>
                </div>
            </div>
            </div>
        </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
                <i class="fas fa-paint-brush"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Belum Bayar Sablon</h4>
                </div>
                <div class="card-body">
                Rp.<?php echo rupiah($sumSablonNot); ?>
                </div>
            </div>
            </div>
        </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
                <i class="fas fa-tint"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Belum Bayar Sublime</h4>
                </div>
                <div class="card-body">
                Rp.<?php echo rupiah($sumSublimeNot); ?>
                </div>
            </div>
            </div>
        </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
                <i class="fas fa-chart-line"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Total Belum Bayar</h4>
                </div>
                <div class="card-body">
                Rp. <?php 
                
                $ttlSSBNot = $sumBordirNot + $sumSablonNot +$sumSublimeNot;

                echo rupiah($ttlSSBNot);
                
                ?>
                </div>
            </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
                <i class="fas fa-chart-line"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Total Omset Bulan ini</h4>
                </div>
                <div class="card-body">
               Rp.<?php echo rupiah($sumBulanini); ?>
                </div>
            </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
                <i class="fas fa-chart-line"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Total Omset Tahun ini</h4>
                </div>
                <div class="card-body">
                Rp.<?php echo rupiah($sumTahunini); ?>
                </div>
            </div>
            </div>
        </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <h4>Grafik Pemasukan</h4>
                <div class="card-header-action">
                <a data-collapse="#mycard-collapse1" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                </div>
        </div>
        <div class="collapse show" id="mycard-collapse1" style="">
            <div class="card-body">
                <div id="in" style="height: 250px;"> </div>
            </div>
        </div>
        </div>
    </div>

     <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <h4>Grafik Belum Lunas</h4>
                <div class="card-header-action">
                <a data-collapse="#mycard-collapse2" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                </div>
        </div>
        <div class="collapse show" id="mycard-collapse2" style="">
            <div class="card-body">
                <div id="blmlunas" style="height: 250px;"> </div>
            </div>
        </div>
        </div>
    </div>
     <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <h4>Grafik Pengeluaran</h4>
                <div class="card-header-action">
                <a data-collapse="#mycard-collapse3" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                </div>
        </div>
        <div class="collapse show" id="mycard-collapse3" style="">
            <div class="card-body">
                <div id="out" style="height: 250px;"> </div>
            </div>
        </div>
        </div>
    </div>
</div>

 <!-- <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <h4>Grafik Pengeluaran</h4>
                <div class="card-header-action">
                <a data-collapse="#mycard-collapse3" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                </div>
        </div>
        <div class="collapse show" id="mycard-collapse3" style="">
            <div class="card-body">
               <canvas id="line-chart" width="800" height="450"></canvas>
            </div>
        </div>
        </div>
    </div>
</div>
 -->


 <div class="card">
    <div class="card-header">
        <h4>Data Belum Lunas</h4>
            <div class="card-header-action">
            <a data-collapse="#mycard-collapse4" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-collapse4" style="">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-1x">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode</th>
                            <th>Nama Pelanggan</th>
                            <th>Nama Pesanan</th>
                            <th>Jenis</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataTagihan->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['tr_kode']; ?></td>
                            <td><?php echo $row['ct_nama']; ?></td>
                            <td><?php echo $row['tr_nama']; ?></td>
                            <td>
                            
                            <?php
                                $status = $row['tr_jenis'];
                                if ($status == 1){
                                echo "<span class='badge badge-success'> Pemasukan </span>";
                                } if ($status == 2){
                                echo "<span class='badge badge-danger'> Pengeluaran </span>";
                                }
                            ?>
                            
                            </td>
                            <td><?php echo rupiah($row['tr_total_harga']); ?></td>
                            <td><div class="badge badge-danger"><?php echo $row['tr_status']; ?></div></td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
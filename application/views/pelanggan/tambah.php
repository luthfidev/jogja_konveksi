
  <div class="section-header">
            <h1>Tambah Pelanggan</h1>
          </div>

          <div class="section-body">
       
         <div class="card">
                    <?php echo form_open('Pelanggan/simpan/','class="needs-validation alert-simpan" novalidate=""'); ?>
                    <div class="card-header">
                      <h4>Input Pelanggan</h4>
                    </div>
                    <div class="card-body">
                       <div class="form-group">
                        <label>Kode Pelangan</label>
                        <input type="text" class="form-control" required="" readonly value="<?php echo $kode; ?>" name="ct_kode"> 
                        <div class="invalid-feedback">
                          Isi Kode Pelanggan
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Nama Pelanggan</label>
                        <input type="text" class="form-control" required="" name="ct_nama">
                        <div class="invalid-feedback">
                        isi nama dengan benar !
                        </div>
                      </div>
                     <div class="form-group mb-0">
                        <label>Alamat</label>
                        <textarea class="form-control" required="" name="ct_alamat"></textarea>
                        <div class="invalid-feedback">
                          isi alamat dengan benar !
                        </div>
                      </div>
                       <div class="form-group">
                        <label>Telepon / WA</label>
                        <input type="text" class="form-control" required="" name="ct_telepon">
                        <div class="invalid-feedback">
                         isi telepon dengan benar !
                        </div>
                      </div>
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Simpan</button>
                    </div>
                <?php echo form_close();?>  
                </div>
        </div>
<div class="section-header">
            <h1>Data Pelanggan</h1>
          </div>

          <div class="section-body">
       
         <div class="card">
          <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">

                        <thead>
                          
                          <tr>

                            <th class="text-center">
                              #
                            </th>
                            <th>Kode Customer</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                    $no=1;
                    foreach ($pelanggan->result_array() as $row) { ?>
                          <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['ct_kode']; ?></td>
                            <td><?php echo $row['ct_nama']; ?></td>
                            <td><?php echo $row['ct_alamat']; ?></td>
                            <td><?php echo $row['ct_telepon']; ?></td>
                            <td><div class="badge badge-success">Active</div></td>
                            <td colspan="2">
                              <a href="<?php echo base_url();?>pelanggan/ubah/<?php echo $row['ct_id'];?>"><i class="fas fa-pencil-alt"></i></a>
                              <a href="#"></a>
                            </td>
                          </tr>
                    <?php
                    $no++;
                    }
                    ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
        </div>
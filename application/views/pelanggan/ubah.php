
<div class="section-header">
    <h1>Ubah</h1>
</div>

          <div class="section-body">
       
         <div class="card">
                    <?php echo form_open('Pelanggan/update/','class="needs-validation alert-simpan" novalidate=""'); ?>
                    <div class="card-header">
                      <h4>Ubah Pelanggan</h4>
                    </div>
                    <input type="hidden" class="form-control" required="" value="<?php echo $row->ct_id; ?>" name="ct_id"> 
                    <div class="card-body">
                       <div class="form-group">
                        <label>Kode Pelangan</label>
                        <input type="text" class="form-control" required="" readonly value="<?php echo $row->ct_kode; ?>" name="ct_kode"> 
                        <div class="invalid-feedback">
                          Isi Kode Pelanggan
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Nama Pelanggan</label>
                        <input type="text" class="form-control" required="" value="<?php echo $row->ct_nama ?>" name="ct_nama">
                        <div class="invalid-feedback">
                        isi nama dengan benar !
                        </div>
                      </div>
                     <div class="form-group mb-0">
                        <label>Alamat</label>
                        <textarea class="form-control" required="" value="<?php echo $row->ct_alamat; ?>" name="ct_alamat"><?php echo $row->ct_alamat; ?></textarea>
                        <div class="invalid-feedback">
                          isi alamat dengan benar !
                        </div>
                      </div>
                       <div class="form-group">
                        <label>Telepon / WA</label>
                        <input type="text" class="form-control" required="" value="<?php echo $row->ct_telepon; ?>" name="ct_telepon">
                        <div class="invalid-feedback">
                         isi telepon dengan benar !
                        </div>
                      </div>
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                <?php echo form_close();?>  
                </div>
        </div>

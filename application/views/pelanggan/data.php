

  <div class="section-header">
            <h1>Data Pelanggan</h1>
          </div>

          <div class="section-body">
       
         <div class="card">
          <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">

                        <thead>
                          
                          <tr>

                            <th class="text-center">
                              #
                            </th>
                            <th>Kode Customer</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                    $no=1;
                    foreach ($pelanggan->result_array() as $row) { ?>
                          <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['ct_kode']; ?></td>
                            <td><?php echo $row['ct_nama']; ?></td>
                            <td><?php echo $row['ct_alamat']; ?></td>
                            <td><?php echo $row['ct_telepon']; ?></td>
                            <td><div class="badge badge-success">Active</div></td>
                            <td colspan="2">
                              <a href="<?php echo base_url();?>pelanggan/ubah/<?php echo $row['ct_id'];?>"><i class="fas fa-pencil-alt"></i></a>
                              <a href="#"></a>
                            </td>
                          </tr>
                    <?php
                    $no++;
                    }
                    ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
        </div>


   
   <!-- Alert Hapus -->
    <script>
        $(document).on("click", ".alert-delete", function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            bootbox.confirm({
                    message: "Menghapus data dapat mengakibatkan data yang bersangkutan akan hilang ?",
                    buttons: {
                        confirm: {
                            label: 'Ya',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Tidak',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                    if (result){
                                    window.location.href = href;
                                                                
                    }
                    }
            });
        });
    </script>
    <!-- Alert Tolak -->
    <script>
        $(document).on("click", ".alert-tolak", function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            bootbox.confirm({
                    message: "Yakin untuk menolak ?",
                    buttons: {
                        confirm: {
                            label: 'Ya',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Tidak',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                    if (result){
                                    window.location.href = href;
                                                                
                    }
                    }
            });
        });
    </script>

    <!-- Alert Approve -->
    <script>
        $(document).on("click", ".alert-approve", function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            bootbox.confirm({
                    message: "Yakin untuk Approve ?",
                    buttons: {
                        confirm: {
                            label: 'Ya',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Tidak',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                    if (result){
                                    window.location.href = href;
                                                                
                    }
                    }
            });
        });
    </script>

        <!-- Alert toPerson -->
    <script>
        $(document).on("click", ".alert-toPerson", function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            bootbox.confirm({
                    message: "Yakin di tambahkan ke data Personal ?",
                    buttons: {
                        confirm: {
                            label: 'Ya',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Tidak',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                    if (result){
                                    window.location.href = href;
                                                                
                    }
                    }
            });
        });
    </script>

        <!-- Alert toCorp -->
    <script>
        $(document).on("click", ".alert-toCorp", function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            bootbox.confirm({
                    message: "Yakin di tambahkan ke data Corporation ?",
                    buttons: {
                        confirm: {
                            label: 'Ya',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Tidak',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                    if (result){
                                    window.location.href = href;
                                                                
                    }
                    }
            });
        });
    </script>

        <script>

    /*$(".alert-simpan").on("submit", function (e) {
    // Init
    var self = $(this);
    e.preventDefault();

    // Show Message        
    var confirm = bootbox.confirm("pastikan data yang anda inputkan sudah benar!!. Ok untuk melanjutkan, Cancel/Batal untuk kembali ke editing data.", function (result) {
        if (!result) {                
            // self.off("click");
            // self.click();
            return confirm ;
            
        }
        self.off("submit");
        self.submit();
        
    });
    });*/


    $(".alert-simpanN").on("submit", function (e) {
    // Init
    var self = $(this);
    e.preventDefault();

    // Show Message        
    var confirm = bootbox.confirm({
    message: "Pastikan data yang anda inputkan sudah benar !!. Data yang sudah di inputkan tidak dapat di ubah/edit !",
    buttons: {
        confirm: {
            label: 'OK',
            className: 'btn-success'
        },
        cancel: {
            label: 'Batal',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
        if (!result) {                
            // self.off("click");
            // self.click();
            return confirm ;
            
        }
        self.off("submit");
        self.submit();
    }
        
    });
    });
</script>
<script>
    $(".alert-simpan").on("submit", function (e) {
    // Init
    var self = $(this);
    e.preventDefault();

    // Show Message        
    var confirm = bootbox.confirm({
    message: "Pastikan data yang anda inputkan sudah benar !!. Data yang sudah di inputkan tidak dapat di hapus !",
    buttons: {
        confirm: {
            label: 'OK',
            className: 'btn-success'
        },
        cancel: {
            label: 'Batal',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
        if (!result) {                
            // self.off("click");
            // self.click();
            return confirm ;
            
        }
        self.off("submit");
        self.submit();
    }
        
    });
    });

    /*  $(document).on("submit", ".alert-simpan", function(e) {
    var confirmation = confirm("pastikan data yang anda inputkan sudah benar!!. Ok untuk melanjutkan, Cancel/Batal untuk kembali ke editing data.") ;

    if (!confirmation)
        {
            e.preventDefault() ;
            returnToPreviousPage();
        }
    return confirmation ;
    }); */
    </script> 
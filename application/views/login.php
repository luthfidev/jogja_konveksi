<?php  if($this->session->userdata("masuk")==(1)) { 
    redirect ("Admin");
 } ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
<title>Login &mdash; Stisla</title>

<!-- General CSS Files -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-4.3.1/dist/css/bootstrap.min.css">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/fontawesome511/css/all.css">
<!-- CSS Libraries -->
<!-- <link rel="stylesheet" href="../node_modules/bootstrap-social/bootstrap-social.css"> -->

<!-- Template CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/components.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/plugins/select2/dist/css/select2.min.css">

</head>

<body>
<div id="app">
<section class="section">
    <div class="container mt-5">
    <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
        <div class="login-brand">
            <img src="../assets/img/stisla-fill.svg" alt="logo" width="100" class="shadow-light rounded-circle">
        </div>

        <div class="card card-primary">
            <div class="card-header"><h4>Login</h4></div>

            <div class="card-body">
            <?php echo form_open('Auth/Login', 'class="needs-validation"'); ?>
                <div class="form-group">
                <label for="email">Username</label>
                <input id="username" type="text" class="form-control" name="username" tabindex="1" required autofocus>
                <div class="invalid-feedback">
                    Please fill in your username 
                </div>
                </div>

                <div class="form-group">
                <div class="d-block">
                    <label for="password" class="control-label">Password</label>
                    <div class="float-right">
                   <!--  <a href="auth-forgot-password.html" class="text-small">
                        Forgot Password?
                    </a> -->
                    </div>
                </div>
                <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                <div class="invalid-feedback">
                    please fill in your password
                </div>
                </div>
                <div class="form-group">
                <label>Tahun</label>
                <select class="form-control select2" name="id_tahun">
                <?php foreach($tahun->result_array() as $row) 
                {
                echo "<option value='$row[tahun]'>$row[tahun]</option>";
                }
                ?>
                </select>
                </div>

                <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                    <label class="custom-control-label" for="remember-me">Remember Me</label>
                </div>
                </div>

                <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                    Login
                </button>
                </div>
                <?php echo form_close();?>
        <!--      <div class="text-center mt-4 mb-3">
                <div class="text-job text-muted">Login With Social</div>
            </div>
            <div class="row sm-gutters">
                <div class="col-6">
                <a class="btn btn-block btn-social btn-facebook">
                    <span class="fab fa-facebook"></span> Facebook
                </a>
                </div>
                <div class="col-6">
                <a class="btn btn-block btn-social btn-twitter">
                    <span class="fab fa-twitter"></span> Twitter
                </a>
                </div>
            </div> -->

            </div>
        </div>
     <!--    <div class="mt-5 text-muted text-center">
            Don't have an account? <a href="auth-register.html">Create One</a>
        </div> -->
        <div class="simple-footer">
            Copyright &copy; Jogja Konveksi 2019
        </div>
        </div>
    </div>
    </div>
</section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/select2/dist/js/select2.full.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery31/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/popperjs/popper.min.js" ></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-4.3.1/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/momentjs/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/stisla.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/morris.js-0.5.1/morris.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- JS Libraies -->
<!-- Template JS File -->
<script src="<?php echo base_url(); ?>assets/admin/js/scripts.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- Page Specific JS File -->
</body>
</html>

<div class="section-header">
    <h1>Data Bahan</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode Bahan</th>
                            <th>Nama</th>
                            <th>Jumlah</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataBahan->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['bh_kode']; ?></td>
                            <td><?php echo $row['bh_nama']; ?></td>
                            <td><?php echo $row['bh_jumlah']; ?></td>
                            <td><?php echo rupiah($row['bh_total_harga']); ?></td>
                            <td><div class="badge badge-success">Active</div></td>
                            <td colspan="2">
                            <!-- <a href="#">Ubah</a> -->
                            <a href="<?php echo base_url();?>bahan/delete/<?php echo $row['bh_id'];?>" class="alert-delete"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

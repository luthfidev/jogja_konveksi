<div class="section-header">
<h1>Tambah Bahan</h1>
</div>

<div class="section-body">       
<div class="card">
<div class="card-body">
<?php echo form_open('Bahan/simpan/','class="needs-validation alert-simpanN" novalidate=""'); ?>
    <div class="row">
    <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
            <h4>Input Bahan</h4>
            </div>
            <div class="card-body">
            <div class="form-group">
                <label>Kode</label>
                <input type="text" class="form-control" value="<?php echo $kode; ?>" name="bh_kode" readonly>
            </div>
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="bh_nama" required>
            </div>
            <div class="form-group">
                <label>Tanggal</label>
                <input type="date" class="form-control" name="bh_tanggal" required>
            </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
            <h4>Input Harga</h4>
            </div>
            <div class="card-body input-harga">
            <div class="form-group">
                <label>Jumlah</label>
                <input type="number" class="form-control" id="jumlah" name="bh_jumlah"  required>
            </div>
            <!-- <div class="form-group">
                <label>Stitch</label>
                <input type="number" class="form-control" id="stitch" name="bh_stitch" required>
            </div>
            <div class="form-group">
                <label>Jumlah Stitch</label>
                <input type="number" class="form-control" id="jmlstitch" name="bh_total_stitch" required>
            </div> -->
            <div class="form-group">
                <label>Harga</label>
                <input type="number" class="form-control" id="harga" name="bh_harga" required>
            </div>
            <div class="form-group">
                <label>Total Harga</label>
                <input type="number" class="form-control" id="ttlharga" name="bh_total_harga" required readonly>
            </div>
            </div>
        </div>
    </div>
    <div class="card-footer text-right">
        <button class="btn btn-primary">Simpan</button>
    </div>
    </div>
<?php echo form_close();?> 
</div>
</div>
</div>
<div class="section-header">
    <h1>Data Bahan</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode Bahan</th>
                            <th>Nama</th>
                            <th>Jumlah</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataBahan->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['bh_kode']; ?></td>
                            <td><?php echo $row['bh_nama']; ?></td>
                            <td><?php echo $row['bh_jumlah']; ?></td>
                            <td><?php echo rupiah($row['bh_total_harga']); ?></td>
                            <td><div class="badge badge-success">Active</div></td>
                            <td colspan="2">
                            <!-- <a href="#">Ubah</a> -->
                            <a href="<?php echo base_url();?>bahan/delete/<?php echo $row['bh_id'];?>" class="alert-delete"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


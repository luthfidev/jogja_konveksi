<div class="section-header">
    <h1>Data Pengeluaran</h1>
</div>

<div class="section-body">       
    <div class="card">
    <div class="card-header">
        <h4>Data Transaksi</h4>
            <div class="card-header-action">
            <a data-collapse="#mycard-collapse5" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-collapse5" style="">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-2">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode</th>
                            <th>Nama Pesanan</th>
                            <th>Jenis</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataTransaksi->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['tr_kode']; ?></td>
                            <td><?php echo $row['tr_nama']; ?></td>
                            <td>
                            
                            <?php
                                $status = $row['tr_jenis'];
                                if ($status == 1){
                                echo "<span class='badge badge-success'> Pemasukan </span>";
                                } if ($status == 2){
                                echo "<span class='badge badge-danger'> Pengeluaran </span>";
                                }
                            ?>
                            
                            </td>
                            <td><?php echo rupiah($row['tr_total_harga']); ?></td>
                            <td><div class="badge badge-success">Active</div></td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
        <h4>Grafik Pengeluaran</h4>
            <div class="card-header-action">
            <a data-collapse="#mycard-collapse4" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
            </div>
        </div>
            <div class="collapse show" id="mycard-collapse4" style="">
                <div class="card-body">
                    <div id="pengeluaran" style="height: 250px;"> </div>
                </div>
            </div>
    </div>

</div>

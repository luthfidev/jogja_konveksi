  <div class="section-header">
    <h1>Data Pengguna</h1>
  </div>

<div class="section-body">       
  <div class="card">
    <div class="card-body">
    
      <div class="row">
        <div class="col-12 col-md-6 col-lg-6">
          <?php echo form_open('Admin/simpanuser/','class="needs-validation alert-simpanN" novalidate=""'); ?>
            <div class="card">
              <div class="card-header">
                <h4>Input Pengguna</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                      <label>Akses Level</label>
                      <select class="form-control select2" name="level_id">
                      <?php foreach($level->result_array() as $row) 
                      {
                        echo "<option value='$row[id_level]'>$row[id_level] | $row[nama_level]</option>";
                      }
                      ?>
                      </select>
                </div>
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" id="username" required>
                   <span id="username_result"></span>
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="password" required>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
          <button id="simpan" class="btn btn-primary">Simpan</button>
        </div>
        <?php echo form_close();?> 
      </div>    
    </div>
  </div>
</div>
<div class="section-header">
    <h1>Data User</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Level</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataUser->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['username']; ?></td>
                            <td>*******************************</td>
                            <td><?php echo $row['nama_level']; ?></td>
                            <td colspan="2">
                            <!-- <a href="#">Ubah</a> -->
                            <?php if ($row['nama_level']=="admin") {?>
                            <a  data-toggle="tooltip" data-placement="right" title="Admin tidak dapat di hapus"><i class="fas fa-trash-alt"></i></i>
                            <!-- <a href="#"  data-toggle="tooltip" data-placement="right" title="Admin tidak dapat di hapus" class="btn btn-danger">Hapus</a> -->
                            <?php }else{ ?>
                            <a href="<?php echo base_url();?>Admin/deleteuser/<?php echo $row['id_user'];?>" class="alert-delete"><i class="fas fa-trash-alt"></i></a>
                            <!-- <td><a href="proses/hapus_user.php?id_admin=<?php echo $data["id_admin"];?>" class="btn btn-danger del">Hapus</a></td> -->
                            <?php } ?>
                          </tr>
                           <!--  <i href="<?php echo base_url();?>Admin/deleteuser/<?php echo $row['id_user'];?>" class="alert-delete"><i class="fas fa-trash-alt"></i></i> -->
                            </td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


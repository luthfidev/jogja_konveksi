

<div class="section-header">
    <h1>Data Sablon</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode Sablon</th>
                            <th>Nama Pelanggan</th>
                            <th>Nama Pesanan</th>
                            <th>Jumlah</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataSablon->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['sb_kode']; ?></td>
                            <td><?php echo $row['ct_nama']; ?></td>
                            <td><?php echo $row['sb_nama_pesanan']; ?></td>
                            <td><?php echo $row['sb_jumlah']; ?></td>
                            <td><?php echo rupiah($row['sb_total_harga']); ?></td>
                            <td><div class="badge badge-info"><?php echo $row['sb_status']; ?></div></td>
                            <td colspan="2">
                            <!-- <a href="#">Ubah</a> -->
                            <a href="<?php echo base_url();?>sablon/delete/<?php echo $row['sb_id'];?>" class="alert-delete"><i class="fas fa-trash-alt"></i></a>


                              <?php 
                                    $ids = 1;
                                    $idss = 2;
                                    if($row['sb_status'] == "Lunas"){
                                  ?>
                                      <a href="<?php echo base_url('sablon/switch/').$row['sb_id'].'/'.$ids; ?>" class="btn btn-xs btn-primary"><i class="fa fa-exchange"></i> Nggak Jadi</a>
                                  <?php 
                                    }elseif($row['sb_status'] == "Belum Lunas"){
                                  ?>
                                      <a href="<?php echo base_url('sablon/switch/').$row['sb_id'].'/'.$idss;?>" class="btn btn-xs btn-primary"><i class="fa fa-exchange"></i>Bayar</a>
                                  <?php
                                    }
                                  ?>
                            </td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

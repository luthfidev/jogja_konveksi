 <div class="card">
    <div class="card-header">
        <h4>Data Belum Lunas</h4>
            <div class="card-header-action">
            <a data-collapse="#mycard-collapse4" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
            </div>
        </div>
        <div class="collapse show" id="mycard-collapse4" style="">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-1x">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode</th>
                            <th>Nama Pelanggan</th>
                            <th>Nama Pesanan</th>
                            <th>Jenis</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataTagihan->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['tr_kode']; ?></td>
                            <td><?php echo $row['ct_nama']; ?></td>
                            <td><?php echo $row['tr_nama']; ?></td>
                            <td>
                            
                            <?php
                                $status = $row['tr_jenis'];
                                if ($status == 1){
                                echo "<span class='badge badge-success'> Pemasukan </span>";
                                } if ($status == 2){
                                echo "<span class='badge badge-danger'> Pengeluaran </span>";
                                }
                            ?>
                            
                            </td>
                            <td><?php echo rupiah($row['tr_total_harga']); ?></td>
                            <td><div class="badge badge-danger"><?php echo $row['tr_status']; ?></div></td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
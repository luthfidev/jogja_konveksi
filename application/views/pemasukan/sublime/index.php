<div class="section-header">
    <h1>Sublime</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
        <?php echo form_open('Sublime/simpan/','class="needs-validation alert-simpanN" novalidate=""'); ?>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                <div class="card-header">
                    <h4>Input Pemesan</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                    <label>Kode</label>
                    <input type="text" class="form-control" value="<?php echo $kode; ?>" name="sbl_kode" readonly>
                    </div>
                    <div class="form-group">
                        <label>Pelanggan</label>
                        <select class="form-control select2" name="pelanggan_id_ct">
                        <?php foreach($pelanggan->result_array() as $row) 
                        {
                            echo "<option value='$row[ct_id]'>$row[ct_kode] | $row[ct_nama]</option>";
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group">
                    <label>Pesanan</label>
                    <input type="text" class="form-control" name="sbl_nama_pesanan" required>
                    </div>
                    <div class="form-group">
                    <label>Datang</label>
                    <input type="text" class="form-control" name="sbl_datang" required id="datepicker_start">
                    </div>
                    <div class="form-group">
                    <label>Kirim</label>
                    <input type="text" class="form-control" name="sbl_kirim" required id="datepicker_end">
                    </div>
                </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                <div class="card-header">
                    <h4>Input Harga</h4>
                </div>
                <div class="card-body input-harga">
                    <div class="form-group">
                    <label>Jumlah</label>
                    <input type="number" class="form-control" id="jumlah" name="sbl_jumlah" required>
                    </div>
                   <!--  <div class="form-group">
                    <label>Stitch</label>
                    <input type="number" class="form-control" id="stitch" name="sbl_stitch" required>
                    </div>
                    <div class="form-group">
                    <label>Jumlah Stitch</label>
                    <input type="number" class="form-control" id="jmlstitch" name="sbl_total_stitch" required>
                    </div> -->
                    <div class="form-group">
                    <label>Harga</label>
                    <input type="text" class="form-control" id="harga" name="sbl_harga" required>
                    </div>
                    <div class="form-group">
                    <label>Total Harga</label>
                    <input type="text" class="form-control" id="ttlharga" name="sbl_total_harga" required readonly>
                    </div>
                     <div class="form-group">
                      <label>Status</label>
                      <select class="form-control" name="sbl_status">
                        <option value="Belum Lunas">Belum Lunas</option>
                        <option value="Lunas">Lunas</option>
                      </select>
                </div>
                </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button class="btn btn-primary">Simpan</button>
            </div>
        </div>
        <?php echo form_close();?> 
        </div>
    </div>
</div>
<div class="section-header">
    <h1>Data Sublime</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode Sublime</th>
                            <th>Nama Pelanggan</th>
                            <th>Nama Pesanan</th>
                            <th>Jumlah</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataSublime->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['sbl_kode']; ?></td>
                            <td><?php echo $row['ct_nama']; ?></td>
                            <td><?php echo $row['sbl_nama_pesanan']; ?></td>
                            <td><?php echo $row['sbl_jumlah']; ?></td>
                            <td><?php echo rupiah($row['sbl_total_harga']); ?></td>
                            <td><div class="badge badge-info"><?php echo $row['sbl_status']; ?></div></td>
                            <td colspan="2">
                            <!-- <a href="#">Ubah</a> -->
                            <a href="<?php echo base_url();?>sublime/delete/<?php echo $row['sbl_id'];?>" class="alert-delete"><i class="fas fa-trash-alt"></i></a>
                             <?php 
                                    $ids = 1;
                                    $idss = 2;
                                    if($row['sbl_status'] == "Lunas"){
                                  ?>
                                      <a href="<?php echo base_url('sublime/switch/').$row['sbl_id'].'/'.$ids; ?>" class="btn btn-xs btn-primary"><i class="fa fa-exchange"></i> Nggak Jadi</a>
                                  <?php 
                                    }elseif($row['sbl_status'] == "Belum Lunas"){
                                  ?>
                                      <a href="<?php echo base_url('sublime/switch/').$row['sbl_id'].'/'.$idss;?>" class="btn btn-xs btn-primary"><i class="fa fa-exchange"></i>Bayar</a>
                                  <?php
                                    }
                                  ?>
                            </td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

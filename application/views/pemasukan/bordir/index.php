<div class="section-header">
    <h1>Bordir</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
        <?php echo form_open('Bordir/simpan/','class="needs-validation alert-simpanN" novalidate=""'); ?>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                <div class="card-header">
                    <h4>Input Pemesan</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                    <label>Kode</label>
                    <input type="text" class="form-control" value="<?php echo $kode; ?>" name="br_kode" readonly>
                    </div>
                    <div class="form-group">
                        <label>Pelanggan</label>
                        <select class="form-control select2" name="pelanggan_id_ct">
                        <?php foreach($pelanggan->result_array() as $row) 
                        {
                            echo "<option value='$row[ct_id]'>$row[ct_kode] | $row[ct_nama]</option>";
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group">
                    <label>Pesanan</label>
                    <input type="text" class="form-control" name="br_nama_pesanan" required>
                    </div>
                    <div class="form-group">
                    <label>Datang</label>
                    <input type="text" class="form-control" name="br_datang" required id="datepicker_start">
                    </div>
                    <div class="form-group">
                    <label>Kirim</label>
                    <input type="text" class="form-control" name="br_kirim" required id="datepicker_end"> 
                    </div>
                </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                <div class="card-header">
                    <h4>Input Harga</h4>
                </div>
                <div class="card-body input-harga">
                    <div class="form-group">
                    <label>Jumlah</label>
                    <input type="number" class="form-control" id="jumlah" name="br_jumlah" required>
                    </div>
                    <div class="form-group">
                    <label>Stitch</label>
                    <input type="number" class="form-control" id="stitch" name="br_stitch" required>
                    </div>
                    <div class="form-group">
                    <label>Jumlah Stitch</label>
                    <input type="number" class="form-control" id="jmlstitch" name="br_total_stitch" required readonly>
                    </div>
                    <div class="form-group">
                    <label>Harga</label>
                    <input type="number" class="form-control" id="harga" name="br_harga" required>
                    </div>
                    <div class="form-group">
                    <label>Total Harga</label>
                    <input type="text" class="form-control" id="ttlharga" name="br_total_harga" required readonly>
                    </div>
                     <div class="form-group">
                      <label>Status</label>
                      <select class="form-control" name="br_status">
                        <option value="Belum Lunas">Belum Lunas</option>
                        <option value="Lunas">Lunas</option>
                      </select>
                </div>
                </div>
                </div>

            </div>
            <div class="card-footer text-right">
                <button class="btn btn-primary">Simpan</button>
            </div>
        </div>
        <?php echo form_close();?> 
        </div>
    </div>
</div>
<div class="section-header">
    <h1>Data Bordir</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode Bordir</th>
                            <th>Nama Pelanggan</th>
                            <th>Nama Pesanan</th>
                            <th>Jumlah</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataBordir->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['br_kode']; ?></td>
                            <td><?php echo $row['ct_nama']; ?></td>
                            <td><?php echo $row['br_nama_pesanan']; ?></td>
                            <td><?php echo $row['br_jumlah']; ?></td>
                            <td><?php echo rupiah($row['br_total_harga']); ?></td>
                            <td><div class="badge badge-info"><?php echo $row['br_status']; ?></div></td>
                            <td colspan="2">
                            <!-- <a href="#">Ubah</a> -->
                            <a href="<?php echo base_url();?>bordir/delete/<?php echo $row['br_id'];?>" class="alert-delete"><i class="fas fa-trash-alt"></i></a>
                              <?php 
                                    $ids = 1;
                                    $idss = 2;
                                    if($row['br_status'] == "Lunas"){
                                  ?>
                                      <a href="<?php echo base_url('bordir/switch/').$row['br_id'].'/'.$ids; ?>" class="btn btn-xs btn-primary"><i class="fa fa-exchange"></i> Nggak Jadi</a>
                                  <?php 
                                    }elseif($row['br_status'] == "Belum Lunas"){
                                  ?>
                                      <a href="<?php echo base_url('bordir/switch/').$row['br_id'].'/'.$idss;?>" class="btn btn-xs btn-primary"><i class="fa fa-exchange"></i>Bayar</a>
                                  <?php
                                    }
                                  ?>
                            </td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


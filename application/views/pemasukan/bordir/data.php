<div class="section-header">
    <h1>Data Bordir</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode Bordir</th>
                            <th>Nama Pelanggan</th>
                            <th>Nama Pesanan</th>
                            <th>Jumlah</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($dataBordir->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['br_kode']; ?></td>
                            <td><?php echo $row['ct_nama']; ?></td>
                            <td><?php echo $row['br_nama_pesanan']; ?></td>
                            <td><?php echo $row['br_jumlah']; ?></td>
                            <td><?php echo rupiah($row['br_total_harga']); ?></td>
                            <td><div class="badge badge-info"><?php echo $row['br_status']; ?></div></td>
                            <td colspan="2">
                            <!-- <a href="#">Ubah</a> -->
                            <a href="<?php echo base_url();?>bordir/delete/<?php echo $row['br_id'];?>" class="alert-delete"><i class="fas fa-trash-alt"></i></a>
                              <?php 
                                    $ids = 1;
                                    $idss = 2;
                                    if($row['br_status'] == "Lunas"){
                                  ?>
                                      <a href="<?php echo base_url('bordir/switch/').$row['br_id'].'/'.$ids; ?>" class="btn btn-xs btn-primary"><i class="fa fa-exchange"></i> Nggak Jadi</a>
                                  <?php 
                                    }elseif($row['br_status'] == "Belum Lunas"){
                                  ?>
                                      <a href="<?php echo base_url('bordir/switch/').$row['br_id'].'/'.$idss;?>" class="btn btn-xs btn-primary"><i class="fa fa-exchange"></i>Bayar</a>
                                  <?php
                                    }
                                  ?>
                            </td>
                        </tr>
                    <?php
                    $no++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

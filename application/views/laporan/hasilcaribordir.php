  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-4.3.1/dist/css/bootstrap.min.css">
<style type="text/css">
    body {
  font-family: Georgia, "Times New Roman", Times, serif;
}
</style>
<div class="section-header">
    <h1>Cetak Laporan</h1>
</div>
<body>
<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <h4>Laporan</h4>
            <div class="table-responsive">
                <table class="table table-striped table-1x">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode</th>
                            <th >Nama Pelanggan</th>
                            <th>QTY</th>
                            <th>Stitch</th>
                            <th>Total Stitch</th>
                            <th>Datang</th>
                            <th>Kirim</th>
                            <th>Harga</th>
                            <th>Total</th>
                            <th>Total Bayar</th>
                            <th>Tgl. Pembayaran</th>
                            <th>No. Nota</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total = 0;
                    $no=1;
                    foreach ($dataKeuangan->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['tr_kode']; ?></td>
                            <td><?php echo $row['tr_nama']; ?></td>
                            <td><?php echo $row['tr_jumlah']; ?></td>
                            <td><?php echo $row['tr_stitch']; ?></td>
                            <td><?php echo $row['tr_total_stitch']; ?></td>
                            <td><?php echo $row['tr_datang']; ?></td>
                            <td><?php echo $row['tr_kirim']; ?></td>                         
                            <td><?php echo rupiah($row['tr_harga']); ?></td>
                            <td><?php echo rupiah($row['tr_total_harga']); ?></td>
                                 <?php $total += $row['tr_total_harga']; ?>
                             <td><?php echo date("Y/m/d"); ?></td>
                            <td><?php echo "nota"; ?></td>
                            <td><?php echo "nota"; ?></td>
                        </tr>
                        
                  <?php
    
                    }
                
                    ?>
                    </tbody>
                         <tr>
                            <td colspan="10">Jumlah</td>
                 
                           
                           <td>Rp.<?php echo rupiah($total); ?></td>
                        </tr>
                    
                </table>
            </div>
        </div>
    </div>
</div>
</body>
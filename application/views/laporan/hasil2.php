  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-4.3.1/dist/css/bootstrap.min.css">
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/fontawesome511/css/all.css"> 
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/plugins/datables4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/plugins/select2/dist/css/select2.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/plugins/morris.js-0.5.1/morris.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/components.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

<link rel="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css">
<div class="section-header">
    <h1>Hasil Pencarian</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <h4>Hasil</h4>
            <div class="table-responsive">
                <table class="table table-striped table-1x">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode</th>
                            <th>Nama </th>
                            <th>Jenis</th>
                            <th>Status</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Total Harga</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total = 0;
                    $no=1;
                    foreach ($dataKeuangan->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['tr_kode']; ?></td>
                            <td><?php echo $row['tr_nama']; ?></td>
                            <td>
                            <?php
                                $jenis = $row['tr_jenis'];
                                if ($jenis == 1){
                                echo "<span class='badge badge-success'> Pemasukan </span>";
                                } if ($jenis == 2){
                                echo "<span class='badge badge-danger'> Pengeluaran </span>";
                                }
                            ?>
                            </td>
                            <td>  <?php
                                $status = $row['tr_status'];
                                if ($status == "Lunas"){
                                echo "<span class='badge badge-success'> Lunas</span>";
                                } if ($status == "Belum Lunas"){
                                echo "<span class='badge badge-danger'> Belum Lunas </span>";
                                }
                            ?>
                            </td>
                            <td><?php echo $row['tr_jumlah']; ?></td>
                            <td><?php echo rupiah($row['tr_harga']); ?></td>
                            <td><?php echo rupiah($row['tr_total_harga']); ?></td>
                            <td><?php echo $row['tr_tanggal']; ?></td>
                                 <?php $total += $row['tr_total_harga']; ?>
                        </tr>
                        
                  <?php
                    $no++;
                    }
                
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="7">Jumlah</th>
                            <th><?php echo rupiah($total); ?></th>
                 
                        </tr>
                    </tfoot>
                    
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>


  <script src="<?php echo base_url(); ?>assets/admin/plugins/popperjs/popper.min.js" ></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-4.3.1/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-nicescroll/jquery.nicescroll.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/momentjs/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/stisla.js"></script>

 
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/select2/dist/js/select2.full.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/morris.js-0.5.1/morris.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <!-- JS Libraies -->
  <!-- Template JS File -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/scripts.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/custom.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootbox/bootbox.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootbox/bootbox.locales.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
    $('.table-1x').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-4.3.1/dist/css/bootstrap.min.css">

<div class="section-header">
    <h1>Cetak Laporan</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
            <h4>Laporan</h4>
            <div class="table-responsive">
                <table class="table table-striped table-1x">
                    <thead>      
                        <tr>
                            <th class="text-center">
                            #
                            </th>
                            <th>Kode</th>
                            <th>Nama </th>
                            <th>Jenis</th>
                            <th>Status</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Total Harga</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total = 0;
                    $no=1;
                    foreach ($dataKeuangan->result_array() as $row) { ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row['tr_kode']; ?></td>
                            <td><?php echo $row['tr_nama']; ?></td>
                            <td>
                            <?php
                                $jenis = $row['tr_jenis'];
                                if ($jenis == 1){
                                echo "<span class='badge badge-success'> Pemasukan </span>";
                                } if ($jenis == 2){
                                echo "<span class='badge badge-danger'> Pengeluaran </span>";
                                }
                            ?>
                            </td>
                            <td>  <?php
                                $status = $row['tr_status'];
                                if ($status == "Lunas"){
                                echo "<span class='badge badge-success'> Lunas</span>";
                                } if ($status == "Belum Lunas"){
                                echo "<span class='badge badge-danger'> Belum Lunas </span>";
                                }
                            ?>
                            </td>
                            <td><?php echo $row['tr_jumlah']; ?></td>
                            <td><?php echo rupiah($row['tr_harga']); ?></td>
                            <td><?php echo rupiah($row['tr_total_harga']); ?></td>
                            <td><?php echo date_ind($row['tr_tanggal']); ?></td>
                                 <?php $total += $row['tr_total_harga']; ?>
                        </tr>
                        
                  <?php
    
                    }
                
                    ?>
                    </tbody>
                         <tr>
                            <td colspan="7">Jumlah</td>
                 
                           
                           <td>Rp.<?php echo rupiah($total); ?></td>
                        </tr>
                    
                </table>
            </div>
        </div>
    </div>
</div>
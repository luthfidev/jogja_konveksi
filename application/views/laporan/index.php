

<div class="section-header">
    <h1>Laporan</h1>
</div>

<div class="section-body">       
    <div class="card">
        <div class="card-body">
        
        <div class="row">
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                <div class="card-header">
                    <h4>Cetak Sablon/Sublime</h4>
                </div>
                <div class="card-body">
                    <?php echo form_open('Laporan/cetakLaporan/','class="needs-validation" novalidate=""'); ?>
                    <div class="form-group">
                        <label>Jenis</label>
                        <select class="form-control select2" name="jenis">
                        <option value="1">Pemasukan</option>
                        <option value="2">Pengeluaran</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control select2" name="status">
                        <option value="Lunas">Lunas</option>
                        <option value="Belum Lunas">Belum Lunas</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jenis Produk</label>
                        <select class="form-control select2" name="jenis_produk">
                        <option value="sablon">Sablon</option>
                        <option value="sublime">Sublime</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tahun</label>
                        <select class="form-control select2" name="tahun">
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Start Date</label>
                        <input type="text" class="form-control"  name="start_date" id="datepicker_start">
                    </div>
                    <div class="form-group">
                        <label>End Date</label>
                        <input type="text" class="form-control" name="end_date" id="datepicker_end">
                    </div>
                </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-primary">Cari</button>  
                </div>
                <?php echo form_close();?> 
            </div>
                <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                <div class="card-header">
                    <h4>Cetak Bordir</h4>
                </div>
                <div class="card-body">
                    <?php echo form_open('Laporan/cetakLaporanBordir/','class="needs-validation" novalidate=""'); ?>
                    <div class="form-group">
                        <label>Jenis</label>
                        <select class="form-control select2" name="jenis">
                        <option value="1">Pemasukan</option>
                        <option value="2">Pengeluaran</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control select2" name="status">
                        <option value="Lunas">Lunas</option>
                        <option value="Belum Lunas">Belum Lunas</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jenis Produk</label>
                        <select class="form-control select2" name="jenis_produk">
                        <option value="bordir">Bordir</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tahun</label>
                        <select class="form-control select2" name="tahun">
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Start Date</label>
                        <input type="text" class="form-control"  name="start_date" id="datepicker_starts">
                    </div>
                    <div class="form-group">
                        <label>End Date</label>
                        <input type="text" class="form-control" name="end_date" id="datepicker_ends">
                    </div>
                </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-primary">Cari</button>
                </div>
                 <?php echo form_close();?> 
            </div>    
        </div>
      
        </div>


    </div>




</div>

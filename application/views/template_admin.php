<?php 
 date_default_timezone_set('Asia/Jakarta');
  if($this->session->userdata("masuk")==(0)) { 
    redirect ("Auth");
 } ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Jogja Konveksi &mdash; Konveksi</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-4.3.1/dist/css/bootstrap.min.css">
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/fontawesome511/css/all.css"> 
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/plugins/datables4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/plugins/select2/dist/css/select2.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/plugins/morris.js-0.5.1/morris.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/components.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
    <?php $this->load->view('layouts/header'); ?>
      
    <?php $this->load->view('layouts/sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
         <?php echo $contents; ?> 
        </section>
      </div>
    <?php $this->load->view('layouts/footer'); ?>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery31/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/popperjs/popper.min.js" ></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-4.3.1/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-nicescroll/jquery.nicescroll.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/momentjs/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/stisla.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/datables4/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/datables4/js/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/select2/dist/js/select2.full.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/morris.js-0.5.1/morris.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <!-- JS Libraies -->
  <!-- Template JS File -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/scripts.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/custom.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootbox/bootbox.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootbox/bootbox.locales.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>


  <?php $this->load->view('alert'); ?>
  <!-- Page Specific JS File -->
  <script type="text/javascript">
  <?php if($this->session->flashdata('sukseslogin')){ ?>
      toastr.options.positionClass = 'toast-top-right';
      toastr.success("<?php echo $this->session->flashdata('sukseslogin'); ?>");
  <?php } if($this->session->flashdata('datasukses')){ ?>
        toastr.options.positionClass = 'toast-top-right';
        toastr.success("<?php echo $this->session->flashdata('datasukses'); ?>");
  <?php } if($this->session->flashdata('datagagal')){?>
        toastr.options.positionClass = 'toast-top-right';
        toastr.error("<?php echo $this->session->flashdata('datagagal'); ?>");
  <?php } if($this->session->flashdata('exist')){?>
        toastr.options.positionClass = 'toast-top-right';
        toastr.error("<?php echo $this->session->flashdata('exist'); ?>");   
  <?php } ?>
</script>
  <script type="text/javascript">
      $('#table-1').DataTable();
      $('.table-1x').DataTable();
      $('.table-2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      'pageLength'  : 5,
      'pagingType'  : 'simple'
    })
  </script>

  <script>
    $('.select2').select2();
     $('#datepicker_start').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    })

     $('#datepicker_starts').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    })

    $('#datepicker_end').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    })

     $('#datepicker_ends').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    })
  </script>
  
  <script>
  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'myfirstchart',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data:  <?php echo $chart1;?>,
  // The name of the data record attribute that contains x-values.
  xkey: 'tahun',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['total'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['total']

});
  </script>

<script>
  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'pengeluaran',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data:  <?php echo $chart2;?>,
  // The name of the data record attribute that contains x-values.
  xkey: 'YEAR(tr_tanggal)',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['total'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['total']
});
  </script>


<script>
  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'in',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data:  <?php echo $chart3;?>,

  // The name of the data record attribute that contains x-values.
  xkey: 'bulan',
  parseTime: false,
  // A list of names of data record attributes that contain y-values.
  ykeys: ['total'],
   lineColors: ['green'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['total']
  
});
  </script>

   <script>
  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'blmlunas',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data:  <?php echo $chart5;?>,

  // The name of the data record attribute that contains x-values.
  xkey: 'bulan',
  parseTime: false,
  // A list of names of data record attributes that contain y-values.
  ykeys: ['total'],
   lineColors: ['red'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['total']
  
});
  </script>

  <script>
  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'out',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data:  <?php echo $chart4;?>,

  // The name of the data record attribute that contains x-values.
  xkey: 'bulan',
  parseTime: false,
  // A list of names of data record attributes that contain y-values.
  ykeys: ['total'],
   lineColors: ['red'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['total']
  
});
  </script>
<!-- <?php
        foreach($chart6 as $data){
            $produk[] = $data->bulan;
            $total[] =  $data->tr_total_harga;
        }
    ?> -->
<!--   <script type="text/javascript">
 new Chart(document.getElementById("line-chart"), {
    type: 'bar',
    data: {
      labels: [  <?php
            if (count($chart6)>0) {
              foreach ($chart6 as $data) {
                echo "'" .$data->tr_produk ."',";
              }
            }
          ?>],
      datasets: [
        {
          label: [  <?php
            if (count($chart6)>0) {
              foreach ($chart6 as $data) {
                echo "'" .$data->tr_produk ."',";
              }
            }
          ?>],
          backgroundColor: "#3e95cd",
          data: [  <?php
            if (count($chart6)>0) {
              foreach ($chart6 as $data) {
                echo "'" .$data->total ."',";
              }
            }
          ?>,0]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Population growth (millions)'
      }
    }
});
  </script> -->

<script type ="text/javascript">
		$(".input-harga").keyup(function(){
			var jumlah = parseInt($("#jumlah").val())
			var harga = parseInt($("#harga").val())
			var stitch = parseInt($("#stitch").val())

			var total = harga * jumlah;
      var totalstitch = stitch * jumlah;
			$("#ttlharga").attr("value",total)
      $("#jmlstitch").attr("value",totalstitch)
			
			});
	</script>

  <script type="text/javascript">
 $(document).ready(function(){
  $('#username').change(function(){
   var username = $('#username').val();
   if(username != ''){
    $.ajax({
     url: "<?php echo base_url(); ?>Admin/checkUsername",
     method: "POST",
     data: {username:username},
     success: function(data){
      $('#username_result').html(data);
     }
    });
   }
  });
 });
</script>

</body>
</html>

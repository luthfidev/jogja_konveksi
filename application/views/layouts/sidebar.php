<div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Jogja Konveksi</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">JK</a>
          </div>
          <ul class="sidebar-menu" >
              <li class="menu-header">Dashboard</li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                  <li class="actives"><a class="nav-link" href="<?php echo base_url(); ?>Dashboard">General Dashboard</a></li>
                  <li><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
                </ul>
              </li>
              <li class="menu-header">Target</li>
                <li ><a class="nav-link" href="<?php echo base_url(); ?>Pelanggan/tambah"><i class="fas fa-users"></i> <span>Pelanggan</span></a></li>
                <li ><a class="nav-link" href="<?php echo base_url(); ?>Sablon"><i class="fas fa-paint-brush"></i><span>Sablon</span></a></li>
                <li ><a class="nav-link" href="<?php echo base_url(); ?>Sublime"><i class="fas fa-tint"></i> <span>Sublime</span></a></li>
                <li ><a class="nav-link" href="<?php echo base_url(); ?>Bordir"><i class="fab fa-steam-symbol"></i> <span>Bordir</span></a></li>
              <li class="menu-header">Pengeluaran</li>
              <li ><a class="nav-link" href="<?php echo base_url(); ?>Bahan/index"><i class="fas fa-boxes"></i> <span>Bahan</span></a></li>
              <li class="menu-header">Transaksi</li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-chart-line"></i> <span>Pemasukan</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="<?php echo base_url(); ?>Pemasukan/data" >Data Pemasukan</a></li>
                  <li><a class="nav-link" href="<?php echo base_url(); ?>Pemasukan/blmlunas">Belum Lunas</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-chart-bar"></i> <span>Pengeluaran</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="<?php echo base_url(); ?>Pengeluaran/data" >Data Pengeluaran</a></li>
                </ul>
              </li>
               <li class="menu-header">Pembukuan</li>
               <li ><a class="nav-link" href="<?php echo base_url(); ?>laporan"><i class="fas fa-chart-line"></i> <span>Laporan</span></a></li>
             <!--  <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-chart-line"></i> <span>Laporan</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="<?php echo base_url(); ?>laporan" >Harian</a></li>
                  <li><a class="nav-link" href="layout-transparent.html">Bulanan</a></li>
                   <li><a class="nav-link" href="layout-transparent.html">Tahunan</a></li>
                </ul>
              </li> -->
              <li ><a class="nav-link" href="blank.html"><i class="far fa-square"></i> <span>Blank Page</span></a></li>
              <li><a class="nav-link" href="credits.html"><i class="fas fa-pencil-ruler"></i> <span>Credits</span></a></li>

              <li class="menu-header">Master</li>
              <li ><a class="nav-link" href="<?php echo base_url(); ?>Admin/adduser"><i class="far fa-square"></i> <span>User</span></a></li>
            </ul>

        
        </aside>
      </div>
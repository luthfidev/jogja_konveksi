<?php
    
    Class PdfGenerator
    {
        public function generate($html,$filename,$paper,$orientation)
        {
            $dompdf = new Dompdf\Dompdf();
            $dompdf->loadHtml($html,'UTF-8');
            $dompdf->setPaper($paper,$orientation);
            $dompdf->render();
            $dompdf->stream($filename, array('Attachment' => 0));
        }

    }


?>